import Reactotron,{openInEditor,asyncStorage} from 'reactotron-react-native';

// const reactotron = Reactotron
Reactotron
    .configure({ name: 'Spltty' }) // controls connection & communication settings
    .useReactNative(openInEditor(), asyncStorage()) // add all built-in react native plugins
    .connect() ;// let's connect!


// export default reactotron