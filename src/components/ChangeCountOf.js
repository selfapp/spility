//changeCountContainer: styles of the overall container
//  subtractStyles: style of the subtract button
// addstyles: style of the add button
// value: value of the count
// changeValue: func to change the value of the count to be passed from parent component

import React, {Component} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

export default class ChangeCountOf extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={[
                styles.Container,
                this.props.changeCountContainer
            ]}>
                <View style={[styles.subtract, this.props.subtractStyles]}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={()=>this.props.changeValue('-',this.props.whichValue)}
                    >
                    <Text>-</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1,width:'60%', alignItems:'center'}}>
                    <Text style={{color:'#fff'}}>{this.props.value}</Text>
                </View>
                <View style={[styles.add,this.props.addstyles]}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={()=>this.props.changeValue('+',this.props.whichValue)}
                    >
                    <Text style={{color:'#fff'}}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Container:{
        height: 40,
        width: '85%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        borderWidth:2,
        borderColor:'#fff',
        flexDirection: 'row'
    },
    subtract:{
        height:30,
        width:30,
        borderRadius: 15,
        borderWidth: 1,
        backgroundColor:'#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5
    },
    add:{
        height:30,
        width:30,
        borderRadius: 15,
        borderWidth: 1,
        backgroundColor:'#F1986E',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5
    },
    button:{
        height:30,
        width:30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

ChangeCountOf.prototypes={
    changeCountContainer:PropTypes.object,
    subtractStyles:PropTypes.object,
    addstyles:PropTypes.object,
    value:PropTypes.number.isRequired,
    changeValue:PropTypes.func.isRequired
};