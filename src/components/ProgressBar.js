import React, {Component} from 'react';
import {View} from 'react-native';


export default class ProgressBar extends Component {
    render() {
        return (
            <View style={[{
                height: 100, width: '100%', alignItems: 'center', justifyContent: 'flex-end',
                // backgroundColor:'red'
            }]}>
                <View style={{backgroundColor: '#94C1FA', height: 3, width: '85%', marginBottom: 20}}/>
            </View>
        );
    }
}

