import { AsyncStorage } from 'react-native';
 let baseURL = 'http://18.220.126.252/';
// let baseURL = 'http://localhost:3000/';
//let baseURL = 'http://192.168.0.130:3000/';

export default class API {
    static baseURL = baseURL;

    static request(url, method = 'GET', body = null) {
        return AsyncStorage.getItem('accessToken').then((data) => {
            let access_token = data;
            return fetch(baseURL + url, {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + (access_token ? access_token : null)
                },
                body: body === null ? null : JSON.stringify(body)
            });
        });
    }

    static login(country_code, phone_number) {
        return this.request('request_sms_verification_code', 'POST', {
            user: {
                phone_number: phone_number,
                country_code: country_code
            }
        });
    }

    static verifyOTP(code, country_code, phone_number) {
        return this.request('verify_sms_verification_code', 'POST', {
            user: {
                verification_code: code,
                phone_number: phone_number,
                country_code: country_code
            }
        });
    }

    static updateProfile(first_name, last_name, description, gender, email, city, state, country, facebook_username, instagram_username, linkedin_username, interests, profile_image, passport) {
        return AsyncStorage.getItem('user').then(user => {
            user = JSON.parse(user);
            return this.request(`users/${user.uuid}`, 'PATCH', {
                user: { first_name, last_name, description, gender, email, city, state, country, facebook_username, instagram_username, linkedin_username, interests, profile_image, passport }
            });
        });
    }

    static getUser() {
        return AsyncStorage.getItem('user').then(user => {
            user = JSON.parse(user);
            return this.request(`users/${user.uuid}`);
        });
    }

    static getUserLocation(lat, lng) {
        return AsyncStorage.getItem('user').then(user => {
            user = JSON.parse(user);
            return this.request(`users/${user.uuid}/get_current_location.json?lat=${lat}&lng=${lng}`)
        });
    }

//    Chat API
    static loadLast10People() {
        return this.request('users.json', 'GET')
    }

//    Posts API
    static createPost(city, country, title, description) {
        let image_number = parseInt(Math.random() * (5 - 1) + 1);
        return this.request('posts', 'POST', {
            post: { city, country, title, description, image: image_number }
        })
    }

    static loadPosts() {
        return this.request('posts');
    }
}
