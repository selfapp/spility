import React, {Component} from 'react';
import {
    Image,
    TouchableOpacity,
    View
} from 'react-native';
import PropTypes from 'prop-types';
import {StackActions, withNavigation} from 'react-navigation';
import {
    BoldText, LightText
} from '../components/styledTexts';

class Header extends Component {
    render() {
        return <View
            style={[{
                height: 50,
                flexDirection: 'row'
            }, this.props.headerContainerStyles]}
        >
            <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>
                {/*Back Button*/}
                {
                    this.props.showBackButton ?
                        <TouchableOpacity
                            style={[{
                                shadowColor: 'black',
                                shadowOffset: {height: 0.5, width: 0.5},
                                shadowRadius: 1
                            }, this.props.backButtonBlackStyles]}
                            onPress={this.props.backFunc ? () => this.props.navigation.dispatch(StackActions.popToTop()) : () => this.props.navigation.goBack()}
                        >
                            {
                                this.props.backButtonBlack ?
                                    <Image
                                        source={require('./../assets/blackHeaderArrow.png')}
                                        style={{ height: 20, width: 20 }}
                                        resizeMode={'contain'}
                                    /> :
                                    <Image
                                        source={require('./../assets/back-button.png')}
                                        style={{ height: 20, width: 20 }}
                                        resizeMode={'contain'}
                                    />
                            }
                        </TouchableOpacity>
                        :
                        null
                }
            </View>
            {/*Title*/}
            <View style={[{flex: 1, alignItems: 'center', justifyContent: 'center'}, this.props.titleContainer]}>
                <BoldText style={[{fontSize: 25, fontWeight: '300', color: "#fff"}, this.props.titleStyle]}>
                    {this.props.title}
                </BoldText>
                {this.props.title2?
                <LightText style={[{fontSize: 14, fontWeight: '300', color: "#fff", top:3}, this.props.titleStyle2]}>
                    {this.props.title2}
                </LightText>
                :null
                }
            </View>

            {/*Filter Button*/}
            <View style={{ flex: 0.25, alignItems: 'center', justifyContent: 'center' }}>
                {
                    this.props.showFilter ?
                        <TouchableOpacity
                            style={[{}, this.props.backButtonBlackStyles]}
                            onPress={() => this.props.navigation.navigate('Filter')}
                        >
                            {this.props.backButtonBlack ?
                                <Image
                                    source={require('./../assets/filterBlack.png')}
                                    style={{ height: 20, width: 20 }}
                                    resizeMode={'contain'}
                                />
                                :
                                <Image
                                    source={require('./../assets/toogle.png')}
                                    style={{ height: 20, width: 20 }}
                                    resizeMode={'contain'}
                                />}
                        </TouchableOpacity>
                        :
                        null
                }
            </View>
        </View>;
    }
}

export default withNavigation(Header)

Header.propTypes = {
    showFilter: PropTypes.bool,
    showBackButton: PropTypes.bool,
    backButtonBlack: PropTypes.bool,
    title: PropTypes.string,
    headerContainerStyles: PropTypes.object,
    titleStyle: PropTypes.object,
    titleContainer: PropTypes.object,
    backButtonBlackStyles: PropTypes.object,
    backFunc: PropTypes.bool,
};

Header.defaultProps = {
    showFilter: false,
    showBackButton: false,
    backButtonBlack: false,
    backFunc: false
};
