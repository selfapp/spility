//   Styles
//dropdownToggleStyles for styles of the touchableOpacityfor showing picker
//pickerViewStyles for styles of the wrapper view
//underlineWidth for styles of the underline under the touchableOpacity
//pickerStyles for styles of the picker on showing picker
//itemStyle for styles of the text in picker on showing picker
//selectedTextStyle for styles of the selectedTextStyle from picker after selecting from picker

//    Data props
//pickerData= array for Picker component. eg ['a','b','c']
//showRightImage = show right icon. Boolean
//showLeftImage= show left icon.  Boolean
// imageLeft= path for icon on Left.  eg imageLeft={require(./../ayz.png)}
// imageRight= path for icon on Right.  eg imageLeft={require(./../ayz.png)}

import React, {Component} from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';

import {Dropdown} from 'react-native-material-dropdown';
// for all the props and callbacks for react-native-material-dropdown
// https://www.npmjs.com/package/react-native-material-dropdown


let height1 = Dimensions.get("window").height;
let width1 = Dimensions.get("window").width;
export default class CustomPicker extends Component{
    constructor(props){
        super(props);
        this.state={
            value: '',
            showPicker: true
        }
    }

    showPicker=()=>{
        this.setState({showPicker:!this.state.showPicker})
    };

    closePicker=(itemValue)=>{
        this.setState({value: itemValue});
        // this.setState({showPicker:!this.state.showPicker});
    };



    render(){

        // let pickerList=
        //     this.props.pickerData.map((s,i)=>
        //
        //             // return(
        //         {/*<Picker.item key={i} label={s} value={s}/>*/
        //         }
        //         // )
        //
        //     );
        return(

            <View style={[{height:60,width:100, },this.props.pickerViewStyles]}>
                {
                    this.props.showLeftImage ?
                        <View
                            style={{
                                position: 'absolute',
                                height: 30,
                                width: 30,
                                top: 12,
                                left: height1 < 700 ? -5 : 5,
                            }}

                        >
                            <Image
                                style={{flex: 1, resizeMode: 'contain', alignSelf: 'center'}}
                                source={this.props.imageLeft}
                            />
                        </View> : null
                }
                {
                    this.props.showRightImage ?
                        <View
                            style={{position: 'absolute', height: 30, width: 30, top: 12, right: -5,}}

                        >
                            <Image
                                style={{flex: 1, resizeMode: 'contain', alignSelf: 'center'}}
                                source={this.props.imageRight}
                            />
                        </View> : null
                }
                <TouchableOpacity
                    style={[{flex:1},this.props.dropdownToggleStyles]} //dropdownToggleStyles for styles of the touchableOpacityfor showing picker
                    // onPress={this.showPicker}
                >
                    {this.state.showPicker ?
                        <Text style={[{color: '#fff', fontSize: 25, marginLeft: 10}, this.props.selectedTextStyle]}>
                        {this.state.value === null? this.props.defaultValue : this.state.value}
                    </Text>:null}
                    {this.props.pickerData && this.state.showPicker ?
                        // {/*<View>*/}
                        //   {/*<Picker*/}
                        //     {/*itemStyle={this.props.itemStyle}*/}
                        //    {/*selectedValue={this.props.initialValue?this.props.initialValue:this.state.value}*/}
                        //    {/*style={[{  width: 100, position:'absolute', bottom:-85 },this.props.pickerStyles]}*/}
                        //    {/*mode={'dropdown'}*/}
                        //    {/*onValueChange={(itemValue, itemIndex) => this.closePicker(itemValue)}>*/}
                        //    {/*{pickerList}*/}
                        // {/*</Picker>*/}
                        // {/*<View style={{height:55, width:70, backgroundColor:'red'}}>*/}
                        <Dropdown
                            data={this.props.pickerData}
                            // value={this.props.initialValue?this.props.initialValue:this.state.value}
                            containerStyle={[{width: 100, position: 'absolute', top: -20}, this.props.pickerStyles]}
                            overlayStyle={[{width: 100, position: 'absolute', top: 70}, this.props.pickerStyles]}
                            inputContainerStyle={{borderBottomColor: 'transparent'}}
                            renderAccessory={false}
                            // onChangeText={({ value }) => this.closePicker(value)}
                            selectedItem={val => alert(val)}
                        />
                        // </View>
                        // </View>
                        : null}

                </TouchableOpacity>
                <View style={[{height: 2, width: 100, backgroundColor: '#94C1FA'}, this.props.underlineWidth]}/>

            </View>
        );
    }
}
CustomPicker.propTypes={
   pickerData: PropTypes.array,
    pickerViewStyles: PropTypes.object,
    dropdownToggleStyles: PropTypes.object,
    underlineWidth: PropTypes.object,
    pickerStyles: PropTypes.object,
    itemStyle: PropTypes.object,
    initialValue: PropTypes.string,
    showLeftImage: PropTypes.bool,
    showRightImage: PropTypes.bool,
    imageLeft: PropTypes.number,
    imageRight: PropTypes.number,
};

CustomPicker.defaultProps={
    pickerData : ['Select'],
};