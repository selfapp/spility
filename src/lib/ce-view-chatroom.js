import React from "react";
import {
    Platform,
    KeyboardAvoidingView,
    SafeAreaView,
    View
} from "react-native";
import styles from "./ce-theme-style";
import MessageList from "./ce-view-messagelist";
import MessageEntry from "./ce-view-messageentry";
import ChatEngineProvider from "./ce-core-chatengineprovider";
import Header from '../components/Header';

class ChatRoom extends React.Component {
    renderContents() {
        let self = this;
        const navigate = this.props.navigation;
        return (
            <View style={{ flex: 1, backgroundColor:'#F7F7F7'}}>
                <Header
                    backFunc={true}
                    titleStyle={{fontSize: 18}}
                    title={this.props.navigation.state.params.name}
                    headerContainerStyles={{backgroundColor: '#3C29B0'}}
                    showBackButton={true}
                    withBackButton
                    navigation={navigate}
                />

                <View style={{ flex: 1, marginBottom: 5, backgroundColor: 'white' }}>
                    <MessageList
                        imageUrl = {this.props.navigation.state.params.imageUrl}
                        ref="MessageList"
                        navigation={self.props.navigation}
                        now={ChatEngineProvider.getChatRoomModel().state.now}
                        chatRoomModel={ChatEngineProvider.getChatRoomModel()}
                    />
                </View>

                <View style={{ flex: 0.1 }}>
                    <MessageEntry
                        imageUrl = {this.props.navigation.state.params.imageUrl}
                        chatRoomModel={ChatEngineProvider.getChatRoomModel()}
                        typingIndicator
                        keyboardVerticalOffset={50}
                    />
                </View>
            </View>
        );
    }

    render() {
        if (Platform.OS === "ios") {
            return (
                <SafeAreaView style={styles.container}>
                    <KeyboardAvoidingView behavior="position" style={{flex:1}}>
                        {this.renderContents()}
                    </KeyboardAvoidingView>
                </SafeAreaView>
            );
        }

        return <View style={styles.container}>{this.renderContents()}</View>;
    }
}

export default ChatRoom;
