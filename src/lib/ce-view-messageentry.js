import {
    TextInput,
    View,
    TouchableOpacity,
    Text,
    Dimensions,
} from "react-native";

import React from "react";
import NameTypingIndicator from "./ce-view-nametypingindicator";
import styles from "./ce-theme-style";
import ChatEngineProvider from "./ce-core-chatengineprovider";
const width = Dimensions.get("window").width;

class MessageEntry extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            chatInput: ""
        };

        this.setChatInput = this.setChatInput.bind(this);
    }

    sendChat() {
        var self = this;

        if (self.state.chatInput) {
            var chat = self.props.chatRoomModel.state.chat;

            chat.emit("message", {
                text: self.state.chatInput,
                imageUrl: this.props.imageUrl,
                sentAt: new Date().toISOString(),
                from: {
                    uuid: ChatEngineProvider._uuid,
                    email: ChatEngineProvider._username,
                    name: ChatEngineProvider._name
                }
            });

            this.setState({chatInput: ""});
        }
    }

    setChatInput(value) {
        this.setState({chatInput: value});

        if (this.props.typingIndicator) {
            var chat = this.props.chatRoomModel.state.chat;

            if (value !== "") {
                chat.typingIndicator.startTyping();
            } else {
                chat.typingIndicator.stopTyping();
            }
        }
    }

    onTypingIndicator() {
        return (
            <NameTypingIndicator
                chatRoomModel={ChatEngineProvider.getChatRoomModel()}
            />
        );
    }

    render() {
        return (

            <View
                style={{
                    flexDirection: "row",
                    width: width, justifyContent: 'center', alignItems: 'center'
                }}
            >
                <View
                    style={{
                        flex: 0.8,
                        borderWidth: 1,
                        borderColor: 'gray',
                        borderRadius: 5,
                        marginLeft: 5
                    }}
                >
                    <TextInput
                        value={this.state.chatInput}
                        style={styles.messageentry}
                        underlineColorAndroid="transparent"
                        placeholder="Send Message"
                        onChangeText={this.setChatInput}
                        autoCorrect={false}
                        onSubmitEditing={() => {
                            //this.sendChat();
                        }}
                    />
                </View>

                <View style={{flex: 0.15}}>
                    <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center'}}
                                      onPress={() => this.sendChat()}>
                        <Text>Send</Text>
                        {/* <Image source={require("./../assets/send.png")} style={{}} /> */}
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

export default MessageEntry;
