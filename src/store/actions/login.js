import React from 'react';
import {
    ACCESS_TOKEN_CHECK_FAILED,
    ACCESS_TOKEN_CHECK_START,
    ACCESS_TOKEN_CHECK_SUCCESS,
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_FAILED
} from './../ActionTypes'
import {AsyncStorage} from 'react-native';

export const accessTokenCheck = () => async dispatch => {
    dispatch({type: ACCESS_TOKEN_CHECK_START, loading: true});
    // const token = "0770655e-5a40-4a27-b5be-d4227cd97b8d";
    const token = await AsyncStorage.getItem('accessToken');
    if (token !== null) {
        dispatch({type: ACCESS_TOKEN_CHECK_SUCCESS, token: token});
    } else {
        dispatch({type: ACCESS_TOKEN_CHECK_FAILED, error: token});
    }
};

export const login = (token) => async dispatch => {
    dispatch({type: LOGIN_START, loading: true});
    await AsyncStorage.setItem('accessToken', token);
    if (token !== null) {
        dispatch({type: LOGIN_SUCCESS, token: token});
    } else {
        dispatch({type: LOGIN_FAILED, error: token});
    }
};
