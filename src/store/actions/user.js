import {
    TYPE_MOBILE,
    CHOOSE_COUNTRY,
    LOGIN_START,
    LOGIN_FAILED,
    LOGIN_ERROR,
    LOGIN_OTP,
    TYPE_FIRST_NAME,
    TYPE_LAST_NAME,
    TYPE_DESCRIPTION,
    TYPE_EMAIL,
    TYPE_CITY,
    TYPE_STATE,
    TYPE_COUNTRY,
    SELECT_GENDER,
    OTP_SUCCESS,
    TYPE_FACEBOOK,
    TYPE_INSTAGRAM,
    TYPE_LINKEDIN,
    END_UPDATE_PROFILE,
    START_UPDATE_PROFILE,
    ERROR_UPDATE_PROFILE,
    REMOVE_INTEREST,
    ADD_INTEREST,
    UPDATE_USER_LOCATION,
    USER_LOGOUT
} from '../ActionTypes';
import API from "../../components/API";
import {AsyncStorage} from "react-native";
import {NavigationActions, StackActions} from "react-navigation";

export const typeMobile = phone_number => {
    return {
        type: TYPE_MOBILE,
        payload: phone_number
    }
};

export const chooseCountry = country_code => {
    return {
        type: CHOOSE_COUNTRY,
        payload: country_code
    }
};

export const login = (country_code, phone_number, dispatch, navigate) => {
    if (phone_number.length === 10) {
        dispatch({
            type: LOGIN_START
        });

        API.login(country_code, phone_number)
            .then(res => res.json())
            .then(jsonRes => {
                if(jsonRes.status) {
                    dispatch({
                        type: LOGIN_OTP
                    });

                    navigate('VerificationCodeCheck', {
                        phoneNumber: phone_number,
                        country_code: country_code
                    });
                } else {
                    dispatch({
                        type: LOGIN_ERROR,
                        payload: {
                            phone_number: [jsonRes.message]
                        }
                    });
                }
            })
            .catch(err => {
                dispatch({
                    type: LOGIN_ERROR,
                    payload: err
                });
            });
    } else {
        dispatch({
            type: LOGIN_ERROR,
            payload: {
                phone_number: ['Kindly enter 10 digit phone number']
            }
        });
    }
};

export const verifyOTP = (code, country_code, phone_number, dispatch, navigation) => {
    if (code.length === 4) {

        dispatch({
            type: LOGIN_START
        });

        try {
            API.verifyOTP(code, country_code, phone_number)
                .then(res => res.json())
                .then(jsonRes => {
                    if(jsonRes.status) {
                        AsyncStorage.setItem('accessToken', jsonRes.access_token)
                            .then(() => {
                                dispatch({ type: OTP_SUCCESS, payload: jsonRes.user.uuid });

                                let final_action = NavigationActions.navigate({
                                    routeName: 'TabNavigator',
                                    action: NavigationActions.navigate({ routeName: 'Profile' })
                                });

                                if(jsonRes.user.first_name == null) {
                                    final_action = NavigationActions.navigate({
                                        routeName: 'CompleteProfile'
                                    });
                                }

                                AsyncStorage.setItem('user', JSON.stringify(jsonRes.user)).then(() => {
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [final_action],
                                        key: null
                                    });
                                    navigation.dispatch(resetAction);
                                });
                            });
                    } else {
                        dispatch({
                            type: LOGIN_ERROR,
                            payload: {
                                otp: [jsonRes.message]
                            }
                        });
                    }
                })
                .catch(err => {
                    dispatch({
                        type: LOGIN_ERROR,
                        payload: err
                    });
                });
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR,
                payload: error
            });
        }
    } else {
        dispatch({
            type: LOGIN_ERROR,
            payload: {
                otp: ['Please enter the correct OTP']
            }
        });
    }
};

export const typeFirstName = first_name => {
    return {
        type: TYPE_FIRST_NAME,
        payload: first_name
    }
};

export const typeLastName = last_name => {
    return {
        type: TYPE_LAST_NAME,
        payload: last_name
    }
};

export const typeDescription = description => {
    return {
        type: TYPE_DESCRIPTION,
        payload: description
    }
};

export const typeEmail = email => {
    return {
        type: TYPE_EMAIL,
        payload: email
    }
};

export const typeCity = city => {
    return {
        type: TYPE_CITY,
        payload: city
    }
};

export const typeState = state => {
    return {
        type: TYPE_STATE,
        payload: state
    }
};

export const typeCountry = country => {
    return {
        type: TYPE_COUNTRY,
        payload: country
    }
};

export const selectGender = gender => {
    return {
        type: SELECT_GENDER,
        payload: gender
    }
};

export const typeFacebook = facebook_username => {
    return {
        type: TYPE_FACEBOOK,
        payload: facebook_username
    }
};

export const typeInstagram = instagram_username => {
    return {
        type: TYPE_INSTAGRAM,
        payload: instagram_username
    }
};

export const typeLinkedin = linkedin_username => {
    return {
        type: TYPE_LINKEDIN,
        payload: linkedin_username
    }
};

export const updateProfile = (first_name, last_name, description, email, city, state, country, gender, profile_image, passport, interests, facebook_username, instagram_username, linkedin_username, navigation, dispatch, reset = true) => {
    if(first_name && last_name && first_name.length > 0 && last_name.length > 0) {
        dispatch({
            type: START_UPDATE_PROFILE
        });
        API.updateProfile(first_name, last_name, description, gender, email, city, state, country, facebook_username, instagram_username, linkedin_username, interests, profile_image, passport)
            .then((res) => res.json())
            .then(jsonRes => {
                if(jsonRes.status) {
                    dispatch({
                        type: END_UPDATE_PROFILE,
                        payload: jsonRes.user
                    });
                    AsyncStorage.setItem('user', JSON.stringify(jsonRes.user)).then(() => {
                        if(reset) {
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [
                                    NavigationActions.navigate({
                                        routeName: 'TabNavigator',
                                        action: NavigationActions.navigate({ routeName: 'Profile' })
                                    })
                                ],
                                key: null
                            });
                            navigation.dispatch(resetAction);
                        } else {
                            navigation.goBack();
                        }
                    });
                } else {
                    dispatch({
                        type: ERROR_UPDATE_PROFILE,
                        payload: jsonRes.errors
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    } else {
        let errors = {};
        if(!first_name) {
            errors['first_name'] = ['Kindly fill the first name'];
        }

        if(!last_name) {
            errors['last_name'] = ['Kindly fill the last name'];
        }

        dispatch({
            type: ERROR_UPDATE_PROFILE,
            payload: errors
        });
    }
};

export const updateProfileFromServer = (data) => {
    return {
        type: END_UPDATE_PROFILE,
        payload: data
    }
};

export const updateInterests = (interests, new_interest, dispatch) => {
    if(interests.indexOf(new_interest) > -1) {
        dispatch({
            type: REMOVE_INTEREST,
            payload: new_interest
        });
    } else {
        dispatch({
            type: ADD_INTEREST,
            payload: new_interest
        });
    }
};

export const updateUserLocation = (x, y, dispatch) => {
    API.getUserLocation(x, y)
        .then(res => res.json())
        .then(jsonRes => {
            dispatch({
                type: UPDATE_USER_LOCATION,
                payload: jsonRes.current_location
            });
        });
};

export const logout = (navigation, dispatch) => {
    AsyncStorage.removeItem('user')
        .then(() => AsyncStorage.removeItem('accessToken'))
        .then(() => {
            dispatch({
                type: USER_LOGOUT
            });

            let final_action = NavigationActions.navigate({
                routeName: 'TabNavigator'
            });

            const resetAction = StackActions.reset({
                index: 0,
                actions: [final_action],
                key: null
            });
            navigation.dispatch(resetAction);
        });
};
