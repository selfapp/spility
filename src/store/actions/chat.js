import {
    CHAT_PEOPLE_LOAD_START,
    CHAT_PEOPLE_LOAD_END, CHAT_OPENING, CHAT_OPENED
} from '../ActionTypes';
import API from '../../components/API';

export const loadLast10People = (dispatch) => {
    dispatch({
        type: CHAT_PEOPLE_LOAD_START
    });

    API.loadLast10People()
        .then(res => res.json())
        .then(jsonRes => {
            dispatch({
                type: CHAT_PEOPLE_LOAD_END,
                payload: jsonRes.users
            });
        })
        .catch(err => {
            dispatch({
                type: CHAT_PEOPLE_LOAD_END,
                payload: []
            });
        });
};

export const chatOpening = () => {
    return {
        type: CHAT_OPENING
    }
};

export const chatOpened = () => {
    return {
        type: CHAT_OPENED
    };
};
