import {
    TYPE_POST_CITY,
    TYPE_POST_COUNTRY,
    TYPE_POST_DESCRIPTION,
    TYPE_POST_TITLE,
    RESET_NEW_POST,
    POST_CREATE_START,
    POST_CREATE_FINISH,
    POST_CREATE_ERROR,
    POSTS_LOAD_START,
    POSTS_LOAD_END,
    POSTS_LOAD_ERROR, ADD_POST
} from "../ActionTypes";
import API from "../../components/API";

export const typeCity = city => {
    return {
        type: TYPE_POST_CITY,
        payload: city
    }
};

export const typeCountry = country => {
    return {
        type: TYPE_POST_COUNTRY,
        payload: country
    }
};

export const typeTitle = title => {
    return {
        type: TYPE_POST_TITLE,
        payload: title
    }
};

export const typeDescription = description => {
    return {
        type: TYPE_POST_DESCRIPTION,
        payload: description
    }
};

export const resetNewPost = () => {
    return {
        type: RESET_NEW_POST
    }
};

export const createPost = (title, city, country, description, image, toast, navigation, dispatch) => {
    dispatch({
        type: POST_CREATE_START
    });

    if(title.length === 0) {
        dispatch({
            type: POST_CREATE_ERROR,
            payload: {}
        });
        toast.show("Title can't be blank", 1000);
    } else if(city.length === 0) {
        dispatch({
            type: POST_CREATE_ERROR,
            payload: {
                city: ["can't be blank"]
            }
        });
        toast.show("City can't be blank", 1000);
    } else if(country.length === 0) {
        dispatch({
            type: POST_CREATE_ERROR,
            payload: {
                country: ["can't be blank"]
            }
        });
        toast.show("Country can't be blank", 1000);
    } else if(description.length === 0) {
        dispatch({
            type: POST_CREATE_ERROR,
            payload: {
                description: ["can't be blank"]
            }
        });
        toast.show("Description can't be blank", 1000);
    } else {
        API.createPost(city, country, title, description)
            .then(res => res.json())
            .then(jsonRes => {
                if(jsonRes.status) {
                    dispatch({
                        type: POST_CREATE_FINISH,
                        payload: jsonRes.post
                    });
                    navigation.goBack();
                } else {
                    dispatch({
                        type: POST_CREATE_ERROR,
                        payload: jsonRes.errors
                    });
                }
            });
    }
};

export const addPost = (post) => {
    return {
        type: ADD_POST,
        payload: post
    }
};

export const loadPosts = (dispatch) => {
    dispatch({
        type: POSTS_LOAD_START
    });

    API.loadPosts()
        .then(res => res.json())
        .then(jsonRes => {
            if(jsonRes.posts) {
                dispatch({
                    type: POSTS_LOAD_END,
                    payload: jsonRes.posts
                });
            }
        })
        .catch(err => {
            dispatch({
                type: POSTS_LOAD_ERROR
            });
        });
};
