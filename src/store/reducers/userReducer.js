import * as actionTypes from '../ActionTypes';

const initialState = {
    uuid: null,
    email: null,
    first_name: null,
    last_name: null,
    city: null,
    state: null,
    country: null,
    phone_number: "",
    country_code: "1",
    description: null,
    interests: [],
    loading: false,
    facebook_username: null,
    instagram_username: null,
    linkedin_username: null,
    profile_image: null,
    passport: null,
    thumb_profile_image: null,
    thumb_passport: null,
    small_profile_image: null,
    small_passport: null,
    current_location: null,
    errors: {}
};

export const userReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.TYPE_MOBILE: {
            return {
                ...state,
                phone_number: action.payload,
                errors: {}
            }
        }

        case actionTypes.CHOOSE_COUNTRY: {
            return {
                ...state,
                country_code: action.payload
            }
        }

        case actionTypes.LOGIN_START: {
            return {
                ...state,
                loading: true,
                errors: {}
            }
        }

        case actionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                loading: false,
                errors: {}
            }
        }

        case actionTypes.LOGIN_ERROR: {
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        }

        case actionTypes.LOGIN_OTP: {
            return {
                ...state,
                loading: false,
                errors: {}
            }
        }

        case actionTypes.LOGIN_FAILED: {
            return {
                ...state,
                loading: false,
                errors: {
                    message: action.payload
                }
            }
        }

        case actionTypes.OTP_SUCCESS: {
            return {
                ...state,
                loading: false,
                uuid: action.payload
            }
        }

        case actionTypes.TYPE_FIRST_NAME: {
            let errors = JSON.parse(JSON.stringify(state.errors));
            if(errors.first_name) {
                delete errors.first_name;
            }

            return {
                ...state,
                first_name: action.payload,
                errors: errors
            }
        }

        case actionTypes.TYPE_LAST_NAME: {
            let errors = JSON.parse(JSON.stringify(state.errors));
            if(errors.last_name) {
                delete errors.last_name;
            }

            return {
                ...state,
                last_name: action.payload,
                errors: errors
            }
        }

        case actionTypes.TYPE_DESCRIPTION: {
            return {
                ...state,
                description: action.payload
            }
        }

        case actionTypes.TYPE_EMAIL: {
            return {
                ...state,
                email: action.payload
            }
        }

        case actionTypes.TYPE_CITY: {
            return {
                ...state,
                city: action.payload
            }
        }

        case actionTypes.TYPE_STATE: {
            return {
                ...state,
                state: action.payload
            }
        }

        case actionTypes.TYPE_COUNTRY: {
            return {
                ...state,
                country: action.payload
            }
        }

        case actionTypes.SELECT_GENDER: {
            return {
                ...state,
                gender: action.payload
            }
        }

        case actionTypes.START_UPDATE_PROFILE: {
            return {
                ...state,
                loading: true
            }
        }

        case actionTypes.TYPE_FACEBOOK: {
            return {
                ...state,
                facebook_username: action.payload
            }
        }

        case actionTypes.TYPE_INSTAGRAM: {
            return {
                ...state,
                instagram_username: action.payload
            }
        }

        case actionTypes.TYPE_LINKEDIN: {
            return {
                ...state,
                linkedin_username: action.payload
            }
        }

        case actionTypes.END_UPDATE_PROFILE: {
            return {
                ...state,
                ...action.payload,
                loading: false
            }
        }

        case actionTypes.ERROR_UPDATE_PROFILE: {
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        }

        case actionTypes.ADD_INTEREST: {
            let interests = state.interests;
            if(!(interests.indexOf(action.payload) > -1)) {
                interests.push(action.payload);
            }
            return {
                ...state,
                interests
            }
        }

        case actionTypes.REMOVE_INTEREST: {
            let interests = state.interests;
            if(interests.indexOf(action.payload) > -1) {
                let index = interests.indexOf(action.payload);
                interests.splice(index, 1);
            }
            return {
                ...state,
                interests
            }
        }

        case actionTypes.UPDATE_USER_LOCATION: {
            return {
                ...state,
                current_location: action.payload
            }
        }

        case actionTypes.USER_LOGOUT: {
            return initialState;
        }

        default:
            return state;
    }
};
