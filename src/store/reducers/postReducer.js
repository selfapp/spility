import * as actionTypes from '../ActionTypes';

const initialState = {
    city: '',
    country: '',
    title: '',
    description: '',
    image_name: '',
    image: '',
    loading: false,
    posts: [],
    errors: {}
};

export const postReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.TYPE_POST_CITY: {
            return {
                ...state,
                city: action.payload
            };
        }

        case actionTypes.TYPE_POST_COUNTRY: {
            return {
                ...state,
                country: action.payload
            };
        }

        case actionTypes.TYPE_POST_TITLE: {
            return {
                ...state,
                title: action.payload
            };
        }

        case actionTypes.TYPE_POST_DESCRIPTION: {
            return {
                ...state,
                description: action.payload
            };
        }

        case actionTypes.POST_CREATE_START: {
            return {
                ...state,
                loading: true
            };
        }

        case actionTypes.POST_CREATE_ERROR: {
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        }

        case actionTypes.POSTS_LOAD_START: {
            return {
                ...state,
                loading: true
            }
        }

        case actionTypes.POSTS_LOAD_END: {
            return {
                ...state,
                loading: false,
                posts: action.payload
            }
        }

        case actionTypes.POSTS_LOAD_ERROR: {
            return {
                ...state,
                loading: false
            }
        }

        case actionTypes.POST_CREATE_FINISH: {
            let posts = JSON.parse(JSON.stringify(state.posts));
            posts.unshift({ ...action.payload });
            return {
                ...state,
                title: '',
                city: '',
                country: '',
                description: '',
                image: '',
                image_name: '',
                loading: false,
                errors: {},
                posts: posts
            };
        }

        case actionTypes.ADD_POST: {
            let posts = JSON.parse(JSON.stringify(state.posts));
            posts.unshift({ ...action.payload });
            return {
                ...state,
                posts: posts
            }
        }

        case actionTypes.RESET_NEW_POST: {
            return {
                ...state,
                title: '',
                city: '',
                country: '',
                description: '',
                image: '',
                image_name: '',
                loading: false,
                errors: {}
            };
        }

        default:
            return state;
    }
};
