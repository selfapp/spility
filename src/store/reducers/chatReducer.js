import * as actionTypes from '../ActionTypes';

const initialState = {
    last10Users: [],
    loading: false
};

export const chatReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.CHAT_PEOPLE_LOAD_START: {
            return {
                ...state,
                loading: true
            };
        }

        case actionTypes.CHAT_PEOPLE_LOAD_END: {
            return {
                ...state,
                loading: false,
                last10Users: action.payload
            };
        }

        case actionTypes.CHAT_OPENING: {
            return {
                ...state,
                loading: true
            }
        }

        case actionTypes.CHAT_OPENED: {
            return {
                ...state,
                loading: false
            }
        }

        default:
            return state;
    }
};
