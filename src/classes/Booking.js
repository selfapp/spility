//https://rest-hotels-api.herokuapp.com/v1/hotels  rest api for hotels

import React, {Component} from 'react';
import {Dimensions, Image, ImageBackground, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Header from "../components/Header";
import ChangeCountOf from "../components/ChangeCountOf";
import DatePicker from 'react-native-datepicker';
import Reactotron from 'reactotron-react-native';
import { BoldText,
    LightText
 } from '../components/styledTexts';


let height = 0;
let width = 0;

const monthNames = ["Jan", "Feb", "Mar", "Ap", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

const daynames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",];
export default class Booking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchValue: '',
            dateFrom: "",
            dateTo: "",
            day: '',
            dateTime: '',
            noOfAdultsValue:0,
            noOfKidsValue:0,

        };
        height = Dimensions.get('window').height;
        width = Dimensions.get('window').width;
    }

    changeState = (val) => {
        this.setState({searchValue: val});
    };
    getday = (val) => {
        try {
            return daynames[val.getDay()];
        }
        catch (e) {
        }
    };

    getmonth = (val) => {
        try {
            return monthNames[val.getMonth()];
        }
        catch (e) {
        }
    };

    getDate = (val) => {
        try {
            return val.getDate();
        }
        catch (e) {
        }
    };

    changeValue=(val,whichVal)=>{
        Reactotron.log("val==----->"+val,this.state[whichVal]);
        val==='-' && this.state[whichVal]>0?
            this.setState({[whichVal]:(this.state[whichVal])-1})
        :null;
        val==='+' ?
            this.setState({[whichVal]:(this.state[whichVal])+1})
            :null;
    };

    render() {

        return (
            <ImageBackground style={{flex: 1}} source={require('./../assets/background.png')}>
                <Header title={"Booking"} showFilter={true} showBackButton={true}/>
                <View style={{flex: 1, backgroundColor: '#00000000', alignItems: 'center',}}>
                    <Text style={{
                        letterSpacing: 3,
                        alignSelf: 'flex-start',
                        fontSize: 30,
                        fontWeight: '500',
                        color: '#fff',
                        marginTop: 40,
                        marginLeft: 30,
                        marginBottom: 30
                    }}>
                        Select a Date
                    </Text>
                    <View style={{flexDirection: 'row', width: 300}}>
                        <View style={{flex: 1}}>
                            <Text style={{marginBottom: 10, color: '#94C1FA', fontWeight: 'bold', marginLeft:5}}>
                                From
                            </Text>
                        </View>
                        <View style={{width: 40, alignItems: 'center', justifyContent: 'flex-end'}}>

                        </View>
                        <View style={{flex: 1}}>
                            <Text style={{marginBottom: 10, color: '#94C1FA', fontWeight: 'bold', marginLeft:10}}>
                                To
                            </Text>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', width: 300, marginTop: 10}}>
                        <View style={{flex: 1}}>
                            <DatePicker
                                style={{width: 200}}
                                date={this.state.date}
                                mode="datetime"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="2016-05-01"
                                // maxDate="2016-06-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                hideText={true}
                                customStyles={{
                                    dateTouchBody: {
                                        width: 100,
                                        marginLeft: 6, borderwidth: 0
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {
                                    this.setState({dateFrom: date})
                                }}
                            >
                                <View style={{flex: 1, width: 142, height: 50,}}>
                                    <Text style={{
                                        fontSize: 30,
                                        color: "#fff",
                                        flexDirection: 'row',
                                    }}> {this.getDate(this.state.dateFrom)} <Text
                                        style={styles.dateStyle}>{this.getmonth(this.state.dateFrom)}</Text></Text>
                                    <Text style={[styles.dayStyle,{paddingLeft:10}]}>{this.getday(this.state.dateFrom)}</Text>
                                </View>
                            </DatePicker>
                            <View style={{
                                height: 2,
                                width: 112,
                                backgroundColor: '#94C1FA',
                                position: 'absolute',
                                top: 55
                            }}/>
                        </View>
                        <View style={{width: 30, height: 50, alignItems: 'center', justifyContent: 'center'}}>
                            <Image
                                // style={{opacity:.5}}
                                source={require('./../assets/whiteArrow.png')}
                            />
                        </View>
                        <View style={{flex: 1}}>
                            <DatePicker
                                // style={{height:'100%'}}
                                date={this.state.date}
                                mode="datetime"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="2016-05-01"
                                // maxDate="2016-06-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                hideText={true}
                                customStyles={{
                                    dateTouchBody: {
                                        marginLeft: 6,
                                        width: 100,
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {
                                    this.setState({dateTo: date})
                                }}
                            >
                                <View style={{flex: 1, width: 142, height: 50}}>
                                    <Text style={{
                                        fontSize: 30,
                                        color: "#fff",
                                        flexDirection: 'row',
                                        paddingLeft: 10
                                    }}> {this.getDate(this.state.dateTo)} <Text
                                        style={styles.dateStyle}>{this.getmonth(this.state.dateTo)}</Text></Text>
                                    <Text style={styles.dayStyle}>{this.getday(this.state.dateTo)}</Text>
                                </View>
                            </DatePicker>
                            <View style={{
                                height: 2,
                                width: 112,
                                backgroundColor: '#94C1FA',
                                position: 'absolute',
                                top: 55,
                                left: 20
                            }}/>
                        </View>

                    </View>
                    <Text style={{
                        letterSpacing: 3,
                        alignSelf: 'flex-start',
                        fontSize: 30,
                        fontWeight: '500',
                        color: '#fff',
                        marginTop: 40,
                        marginLeft: 30,
                        marginBottom: 30
                    }}>
                        Guest
                    </Text>
                    <View style={{flexDirection: 'row', width: 300}}>
                        <View style={{flex: 1}}>
                            <Text style={{marginBottom: 10, color: '#94C1FA', fontWeight: 'bold'}}>
                                ADULTS
                            </Text>
                        </View>
                        <View style={{width: 40, alignItems: 'center', justifyContent: 'flex-end'}}>

                        </View>
                        <View style={{flex: 1}}>
                            <Text style={{marginBottom: 10, color: '#94C1FA', fontWeight: 'bold'}}>
                                KIDS
                            </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',width: 300, marginTop: 10}}>
                        <View style={[styles.countContainer,{marginRight:10}]}>

                            <ChangeCountOf changeValue={this.changeValue} whichValue={'noOfAdultsValue'} value={this.state.noOfAdultsValue}/>
                        </View>
                        <View style={[styles.countContainer,{marginLeft:10}]}>
                            <ChangeCountOf changeValue={this.changeValue} whichValue={'noOfKidsValue'} value={this.state.noOfKidsValue}/>
                        </View>
                    </View>
                </View>
                {/*<View style={{ width:300, height:30, alignSelf:'center', marginBottom:40}}>*/}
                    <TouchableOpacity
                        style={{width:300, height:50, marginBottom:40, backgroundColor:'#fff', alignSelf:'center', alignItems: 'center', justifyContent: 'center',borderRadius:25}}
                    >
                        <Text style={{color:'#4D55AA'}}>Proceed</Text>
                    </TouchableOpacity>
                {/*</View>*/}

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    searchDestination: {
        height: Dimensions.get('window').height < 1136 ? 70 : 50,
    },
    dateStyle: {
        color: '#fff',
        fontSize: 20,
        paddingLeft: 20
    },
    dayStyle: {paddingLeft: 20, color: "#94C1FA"},
    countContainer:{flex:1, alignItems: 'center', justifyContent: 'center'}
});
