import React, {Component} from 'react';
import {ActivityIndicator, Dimensions, ImageBackground} from 'react-native';
import {withNavigation} from 'react-navigation';

let height1 = Dimensions.get('window').height;
let width1 = Dimensions.get('window').width;

class Auth extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        // this.props.accessTokenCheck();

    }

    render() {
        const navigate = this.props.navigation.navigate;
        return (
            // this.props.errors === true || this.props.loading === false?
            //     navigate('Login'):
            // // console.log('this.props.errors'+this.props.error + this.props.loading)
            // //     :
            <ImageBackground style={{flex: 1, alignitems: 'center', justifyContent: 'center'}}
                             source={require('./../assets/background.png')}>
                <ActivityIndicator
                    size={'large'}
                    color={'white'}
                />
                {/*<Text>{this.props.data}</Text>*/}
            </ImageBackground>
        );
    }
}

// export default connect(
//     state=>({
//         loading:state.tokenCheckReducer.loading,
//         errors:state.tokenCheckReducer.errors,
//         data:state.tokenCheckReducer.data
//     },
//             {accessTokenCheck}
//     )
// )(Auth);
export default withNavigation(Auth);
