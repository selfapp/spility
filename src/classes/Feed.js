import React, {Component} from 'react';
import {
    FlatList,
    Image,
    ImageBackground,
    TouchableOpacity,
    View
} from 'react-native';
import { connect } from "react-redux";
import PubNubReact from 'pubnub-react';
import API from '../components/API';
import {
    BoldText,
} from '../components/styledTexts';
import {addPost, loadPosts} from "../store/actions/post";

const images = {
    Brunei: {
        1: require('../assets/post/Brunei/brunei1.jpg'),
        2: require('../assets/post/Brunei/brunei2.jpg'),
        3: require('../assets/post/Brunei/brunei3.jpg'),
        4: require('../assets/post/Brunei/brunei4.jpg'),
        5: require('../assets/post/Brunei/brunei5.jpg')
    },
    Cambodia: {
        1: require('../assets/post/Cambodia/Cambodia1.jpg'),
        2: require('../assets/post/Cambodia/Cambodia2.jpg'),
        3: require('../assets/post/Cambodia/Cambodia3.jpg'),
        4: require('../assets/post/Cambodia/Cambodia4.jpg'),
        5: require('../assets/post/Cambodia/Cambodia5.jpg')
    },
    Indonesia: {
        1: require('../assets/post/Indonesia/Indonesia1.jpg'),
        2: require('../assets/post/Indonesia/Indonesia2.jpg'),
        3: require('../assets/post/Indonesia/Indonesia3.jpg'),
        4: require('../assets/post/Indonesia/Indonesia4.jpg'),
        5: require('../assets/post/Indonesia/Indonesia5.jpg')
    },
    Laos: {
        1: require('../assets/post/Laos/Laos1.jpg'),
        2: require('../assets/post/Laos/Laos2.jpg'),
        3: require('../assets/post/Laos/Laos3.jpg'),
        4: require('../assets/post/Laos/Laos4.jpg'),
        5: require('../assets/post/Laos/Laos5.jpg')
    },
    Malaysia: {
        1: require('../assets/post/Malaysia/Malaysia1.jpg'),
        2: require('../assets/post/Malaysia/Malaysia2.jpg'),
        3: require('../assets/post/Malaysia/Malaysia3.jpg'),
        4: require('../assets/post/Malaysia/Malaysia4.jpg'),
        5: require('../assets/post/Malaysia/Malaysia5.jpg')
    },
    Myanmar: {
        1: require('../assets/post/Myanmar/1.jpg'),
        2: require('../assets/post/Myanmar/2.jpg'),
        3: require('../assets/post/Myanmar/3.jpg'),
        4: require('../assets/post/Myanmar/4.jpg'),
        5: require('../assets/post/Myanmar/5.jpg')
    },
    Philippines: {
        1: require('../assets/post/Philippines/Bitmap1.jpg'),
        2: require('../assets/post/Philippines/2.jpg'),
        3: require('../assets/post/Philippines/3.jpg'),
        4: require('../assets/post/Philippines/5.jpg'),
        5: require('../assets/post/Philippines/5.jpg')
    },
    Singapore: {
        1: require('../assets/post/Singapore/1.jpg'),
        2: require('../assets/post/Singapore/2.jpg'),
        3: require('../assets/post/Singapore/3.jpg'),
        4: require('../assets/post/Singapore/4.jpg'),
        5: require('../assets/post/Singapore/5.jpg')
    },
    Thailand: {
        1: require('../assets/post/Thailand/1.png'),
        2: require('../assets/post/Thailand/2.jpg'),
        3: require('../assets/post/Thailand/3.jpg'),
        4: require('../assets/post/Thailand/4.jpg'),
        5: require('../assets/post/Thailand/5.jpg')
    },
    Vietnam: {
        1: require('../assets/post/Vietnam/1.jpg'),
        2: require('../assets/post/Vietnam/2.jpg'),
        3: require('../assets/post/Vietnam/3.jpg'),
        4: require('../assets/post/Vietnam/4.jpg'),
        5: require('../assets/post/Vietnam/5.jpg')
    }
};

class Feed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchResults: null
        };

        this.pubnub = new PubNubReact({
            subscribeKey: 'sub-c-8d9c98dc-2b6d-11e9-8f8d-ba64428934ec'
        });
        this.pubnub.init(this);
    }

    componentWillMount() {
        this.pubnub.addListener({
            message: (message) => {
                let post =  JSON.parse(message.message);
                this.props.addPost(post);
            }
        });

        this.pubnub.subscribe({
            channels: ['feeds']
        });

        console.log(images);
    }

    componentWillUnmount() {
        this.pubnub.unsubscribeAll();
    }

    async componentDidMount() {
        this.props.loadPosts();
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#4C4C4C'}}>
                <TouchableOpacity
                    style={{ position: 'absolute', bottom: 20, right: 20, zIndex: 10 }}
                    onPress={() => this.props.navigation.navigate('NewPost')}
                >
                    <Image
                        source={require('../assets/feed/post.png')}
                        style={{ height: 50, width: 50 }}
                    />
                </TouchableOpacity>
                <View style={{flex: 1}}>
                    <FlatList
                        data={this.props.posts}
                        extraData={this.props.posts[0] ? this.props.posts[0].uuid : ''}
                        keyExtractor={(item, index) => index.toString()}
                        style={{flex: 1}}
                        renderItem={({item}) =>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity
                                    style={{ flex: 1 }}
                                    // onPress={() => this.props.navigation.navigate('hotelNavigationOptions', {item: item})}
                                >
                                    <ImageBackground
                                        style={{ height: 180, flex: 1 }}
                                        source={images[item.country][item.image]}
                                    >
                                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginVertical: 25 }}>
                                            <View style={{ width: 150, height: 34, borderRadius: 17, right: -15, justifyContent: 'center', alignItems: 'center', backgroundColor: 'black' }}>
                                                <BoldText style={{ fontSize: 18, color: 'white' }}>
                                                    Get For $ 50
                                                </BoldText>
                                            </View>
                                        </View>
                                    </ImageBackground>
                                </TouchableOpacity>

                                <View style={{ flex: 1, backgroundColor:'black' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, marginVertical: 3 }}>
                                        <BoldText
                                            style={{
                                                fontSize: 18,
                                                color: '#fff',
                                                marginVertical: 5
                                            }}
                                        >
                                            {item.title}
                                        </BoldText>

                                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                            <Image
                                                style={{ height: 15, width: 15 }}
                                                source={require("./../assets/feed/clock-circle-anticon.png")}
                                            />
                                            <BoldText
                                                style={{
                                                    fontSize: 9,
                                                    color: '#fff',
                                                }}
                                            > Today at 4:7 AM
                                            </BoldText>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1, height: 1, marginHorizontal: 10, backgroundColor: 'rgba(255,255,255, 0.5)' }}/>

                                    <View style={{ flex: 1, flexDirection:'row', marginHorizontal: 10 }}>
                                        <View style={{ flex: 1, flexDirection:'row', justifyContent: 'space-between' }}>
                                            <TouchableOpacity style={{ flex: 1,marginVertical: 10 }}>
                                                <Image
                                                    style={{ height: 25, width: 25 }}
                                                    source={require("./../assets/feed/chat_icon.png")}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ flex: 1,marginVertical: 10 }}
                                                              // onPress={() => this.props.navigation.navigate('hotelNavigationOptions', {item: item})}
                                            >
                                                <Image
                                                    style={{ height: 25, width: 25 }}
                                                    source={require("./../assets/feed/infomation_icon.png")}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ flex: 1,marginVertical: 10 }}>
                                                <Image
                                                    style={{ height: 25, width: 25 }}
                                                    source={require("./../assets/feed/Profile_icon.png")}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ flex: 1,marginVertical: 10 }}>
                                                <Image
                                                    style={{ height: 25, width: 25 }}
                                                    source={require("./../assets/feed/invite_icon.png")}
                                                />
                                            </TouchableOpacity>
                                        </View>

                                        <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <Image
                                                style={{ height: 15, width: 15 }}
                                                source={require("./../assets/feed/location_city-material.png")}
                                            />
                                            <BoldText
                                                style={{ fontSize: 14, color: '#fff' }}
                                            > {item.city}, {item.country}
                                            </BoldText>
                                        </View>
                                    </View>

                                </View>

                            </View>
                        }
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        posts: state.post.posts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        loadPosts: () => loadPosts(dispatch),
        addPost: (post) => dispatch(addPost(post))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
