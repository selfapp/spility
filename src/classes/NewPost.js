import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    TextInput,
    Text
} from 'react-native';
import { connect } from "react-redux";
import Ionicon from 'react-native-vector-icons/Ionicons';
import Toast from 'react-native-easy-toast';
import CountryPicker from 'react-native-country-picker-modal';
import {BoldText, LightText} from "../components/styledTexts";
import {
    createPost,
    resetNewPost,
    typeCity,
    typeCountry,
    typeDescription,
    typeTitle
} from "../store/actions/post";
import Loader from '../components/Loader';

let countryList = ["BN", "KH", "ID", "LA", "MY", "MM", "PH", "SG", "TH", "VN"];

class NewPost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cca2: 'US'
        }
    }

    render() {
        return <View style={{ backgroundColor: 'white', flex: 1 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 20 }}>
                <TouchableOpacity
                    onPress={() => {
                        this.props.resetPost();
                        this.props.navigation.goBack()
                    }}
                >
                    <Ionicon name={'ios-close'} size={40} color={'rgb(154, 39, 196)'} />
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ backgroundColor: 'rgb(154, 39, 196)', paddingHorizontal: 20, height: 30, justifyContent: 'center', borderRadius: 15 }}
                    onPress={() => this.props.createPost(this.props.title, this.props.city, this.props.country, this.props.description, this.props.image, this.refs.toast, this.props.navigation)}
                >
                    <BoldText style={{ color: 'white', fontSize: 16 }}>Post</BoldText>
                </TouchableOpacity>
            </View>

            <View style={{ flex: 1, backgroundColor: 'rgb(243, 245, 246)', padding: 20 }}>
                <LightText>Your destination</LightText>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <TextInput
                        style={{ flex: 1, height: 40, backgroundColor: 'white', paddingHorizontal: 5, marginRight: 10, borderRadius: 5 }}
                        placeholder={'City'}
                        value={this.props.city}
                        onChangeText={city => this.props.typeCity(city)}
                    />
                    <CountryPicker
                        onChange={value => {
                            this.props.typeCountry(value.name);
                            this.setState({
                                cca2: value.cca2
                            })
                        }}
                        cca2={this.state.cca2}
                        translation="eng"
                        styles={{ container: { flex: 1, marginRight: 10, paddingHorizontal: 5, backgroundColor: 'white' } }}
                        countryList={countryList}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center', height: 40, paddingHorizontal: 5, justifyContent: 'space-between' }}>
                            {
                                this.props.country.length > 0 ?
                                    <Text style={{ color: 'black' }}>{this.props.country}</Text>
                                    :
                                    <Text style={{ color: 'rgb(187, 187, 193)' }}>Country</Text>
                            }
                            <Ionicon name={'md-arrow-dropdown'} size={25} color={'rgb(187, 187, 193)'}/>
                        </View>
                    </CountryPicker>
                    <View style={{ backgroundColor: 'white', justifyContent: 'center', width: 40, alignItems: 'center' }}>
                        <Ionicon name={'md-locate'} size={25} color={'rgb(154, 39, 196)'}/>
                    </View>
                </View>

                <LightText style={{ marginTop: 20 }}>Title</LightText>
                <TextInput
                    style={{ height: 40, backgroundColor: 'white', paddingHorizontal: 5, marginTop: 10, borderRadius: 5 }}
                    placeholder={'Enter title'}
                    value={this.props.title}
                    onChangeText={title => this.props.typeTitle(title)}
                />

                <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'flex-end' }}>
                    <LightText style={{  }}>Description</LightText>
                    <LightText style={{ color: 'red', fontSize: 12 }}>(200 character limit)</LightText>
                </View>
                <TextInput
                    style={{ flex: 1, backgroundColor: 'white', marginTop: 10, borderRadius: 5, paddingHorizontal: 5 }}
                    placeholder={'What would you link to do...'}
                    multiline={true}
                    maxLength={200}
                    value={this.props.description}
                    onChangeText={description => this.props.typeDescription(description)}
                />
            </View>
            <Loader loading={this.props.loading}/>
            <Toast position='center' ref="toast"/>
        </View>;
    }
}

const mapStateToProps = state => {
    return {
        city: state.post.city,
        country: state.post.country,
        title: state.post.title,
        description: state.post.description,
        image: state.post.image,
        image_name: state.post.image_name,
        loading: state.post.loading,
        errors: state.post.errors,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        typeTitle: title => dispatch(typeTitle(title)),
        typeCountry: country => dispatch(typeCountry(country)),
        typeCity: city => dispatch(typeCity(city)),
        typeDescription: description => dispatch(typeDescription(description)),
        resetPost: () => dispatch(resetNewPost()),
        createPost: (title, city, country, description, image, toast, navigation) => createPost(title, city, country, description, image, toast, navigation, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);
