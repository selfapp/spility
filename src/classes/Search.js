import React, {Component} from 'react';
import {
    Dimensions,
    Image,
    ImageBackground,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    AsyncStorage,
    StatusBar
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { connect } from 'react-redux';
import Header from "../components/Header";
import { BoldText } from '../components/styledTexts';
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";
import {updateProfileFromServer} from "../store/actions/user";

let height1 = Dimensions.get('window').height;
const monthNames = ["Jan", "Feb", "Mar", "Ap", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchValue: '',
            dateFrom: "",
            dateTo: "",
            day: '',
            dateTime: '',
            my_Id: ''
        };
    }

    componentDidMount(){
        this.setPubNub()
    }

    setPubNub() {
        // this.setState({
        //     loader: true
        // })

        AsyncStorage.getItem('user').then((data)=> {
            if(data) {
                let userObject = JSON.parse(data);
                let userId = userObject.phone_number;

                this.setState({my_Id:userId});
                this.props.updateProfileFromServer(userObject);
            }
        });

        setTimeout(() => {
            if(this.state.my_Id) {
                ChatEngineProvider.connect(
                    this.state.my_Id,
                    this.state.my_Id
                ).then(async () => {
                    console.log('chat connected');
                    try {
                        AsyncStorage.setItem(
                            ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY,
                            JSON.stringify({
                                username: "rajeev",
                                name: "rajeev"
                            })
                        );
                    } catch (error) {
                        console.log("error during chat initailise " + error);
                    }
                });
            }
        }, 1000);
    }

    changeState = (val) => {
        this.setState({searchValue: val});
    };

    getDay = (val) => {
        try {
            return dayNames[val.getDay()];
        } catch (e) {
        }
    };

    getMonth = (val) => {
        try {
            return monthNames[val.getMonth()];
        } catch (e) {
        }
    };

    getDate = (val) => {
        try {
            return val.getDate();
        } catch (e) {
        }
    };

    render() {
        const navigate = this.props.navigation.navigate;
        return (
            <ImageBackground style={{flex: 1}} source={require('./../assets/background.png')}>
                <Header title={"Search"} showFilter={true} showBackButton={true}/>
                <View style={{flex: 1, marginVertical: 10, marginHorizontal: 20}}>
                    <View style={{flex: 1, marginVertical: 20}}>
                        <BoldText style={{
                            letterSpacing: 2,
                            fontSize: height1 < 700 ? 20 : 30,
                            fontWeight: '400',
                            color: 'white',
                            flexWrap: 'wrap',
                            flex: 1
                        }}>Let's pick your next destination</BoldText>
                    </View>

                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <View style={{height: 40}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                <Image
                                    source={require('./../assets/menu-footer/search.png')}
                                />

                                <TextInput
                                    value={this.state.searchValue}
                                    style={{fontSize: 20, marginLeft: 10, color: 'white'}}
                                    onChangeText={val => this.setState({searchValue: val})}
                                    placeholder={'Type Destination'}
                                    placeholderTextColor={ 'rgba(255,255,244,0.5)'}
                                    fontFamily={'Nexalight'}
                                />
                            </View>
                            <View style={{marginTop: 10, height: 2, backgroundColor: 'rgba(255,255,244,0.3)'}}/>
                        </View>
                    </View>


                    <View style={{ flex: 1, flexDirection: 'row', marginVertical: 30, maxHeight: 60 }}>
                        <View style={{ flex: 1 }}>
                            <BoldText style={{ color: '#fff', fontWeight: 'bold' }}>
                                From
                            </BoldText>

                            <DatePicker
                                style={{width: 200}}
                                date={this.state.date}
                                mode="datetime"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="2016-05-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                hideText={true}
                                customStyles={{
                                    dateTouchBody: {
                                        width: 100,
                                        marginLeft: 6,
                                        borderwidth: 0
                                    }
                                }}
                                onDateChange={(date) => {
                                    this.setState({dateFrom: date})
                                }}
                            >
                                <View style={{ flex: 1 }}>
                                    <BoldText style={{fontSize: 30, color: "#fff", flexDirection: 'row'}}>
                                        {this.getDate(this.state.dateFrom)}
                                        <BoldText style={styles.dateStyle}>{this.getMonth(this.state.dateFrom)}</BoldText>
                                    </BoldText>
                                    <BoldText style={styles.dayStyle}>{this.getDay(this.state.dateFrom)}</BoldText>
                                </View>
                            </DatePicker>

                            <View style={{height: 2, backgroundColor: 'rgba(255,255,244,0.3)' }}/>
                        </View>

                        <View style={{flex: 0.25, alignItems: 'center', justifyContent: 'center', marginHorizontal: 5}}>
                            <Image
                                source={require('./../assets/arrow.png')}
                            />
                        </View>


                        <View style={{flex: 1}}>
                            <BoldText
                                style={{
                                    color: '#fff',
                                    fontWeight: 'bold'
                                }
                                }>
                                To
                            </BoldText>
                            <DatePicker
                                date={this.state.date}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="2016-05-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                hideText={true}
                                customStyles={{
                                    dateTouchBody: {
                                        marginLeft: 6,
                                        width: 100,
                                    }
                                }}
                                onDateChange={(date) => {
                                    this.setState({dateTo: date})
                                }}
                            >
                                <View style={{flex: 1}}>
                                    <BoldText style={{
                                        fontSize: 30,
                                        color: "#fff",
                                        flexDirection: 'row',
                                        paddingLeft: 10
                                    }}> {this.getDate(this.state.dateTo)} <BoldText
                                        style={styles.dateStyle}>{this.getMonth(this.state.dateTo)}</BoldText></BoldText>
                                    <BoldText
                                        style={[styles.dayStyle, {paddingLeft: 20}]}>{this.getDay(this.state.dateTo)}</BoldText>
                                </View>
                            </DatePicker>
                            <View style={{height: 2, backgroundColor: 'rgba(255,255,244,0.3)'}}/>
                        </View>
                    </View>

                    <View style={{flex: 0.8, alignItems: 'flex-end'}}>
                        <TouchableOpacity
                            onPress={() => navigate('SearchResults')}
                        >
                            <Image
                                style={{
                                    height: 70,
                                    width: 70
                                }}
                                source={require('../assets/forward.png')}
                                resizeMode='contain'
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1}} />
                </View>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    searchDestination: {
        height: Dimensions.get('window').height < 1136 ? 70 : 50,
    },
    dateStyle: {
        color: '#fff',
        fontSize: 20,
        paddingLeft: 20
    },
    dayStyle: {paddingLeft: 10, color: "#94C1FA"}
});

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateProfileFromServer: data => dispatch(updateProfileFromServer(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
