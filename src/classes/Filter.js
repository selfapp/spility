import React, {Component} from 'react';
import {Dimensions, Image, ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import RangeSlider from 'react-native-range-slider'
import Header from './../components/Header';
import CustomPicker from './../components/CustomPicker';
import ProgressBar from './../components/ProgressBar';
import { BoldText, LightText } from '../components/styledTexts';


let height1 = Dimensions.get("window").height;
export default class Filter extends Component{
    constructor(props){
        super(props);
        this.state={
            language: '',
            minimum: 0,
            maximum: 200,
        }
    }
    render(){
        return(
            <ImageBackground style={{flex:1}} source={require('./../assets/background.png')}>
                <Header showFilter={false} showBackButton={true} title={"Filter"}/>
                <View style={{flex:1, marginHorizontal: 20, marginVertical: 30 }}>
                    <BoldText style={{
                        alignSelf: 'flex-start',
                        letterSpacing: 2,
                        fontSize: 20,
                        fontWeight: '500',
                        color: '#fff',
                       
                    }}>
                        Place
                    </BoldText>

                    <View style={{ flex: 1, flexDirection: 'row', marginVertical: 10 }}>
                        <View style={{ flex: 1 }}>
                            <LightText style={{fontSize: 10, color: '#fff', letterSpacing: 2}}>
                                TYPE
                            </LightText>
                            <CustomPicker pickerViewStyles={{width: height1 < 700 ? 150 : 170}}
                                          underlineWidth={{width: height1 < 700 ? 150 : 170}}
                                          pickerStyles={{width: height1 < 700 ? 150 : 170}}
                                          showRightImage={true}
                                          imageRight={require('./../assets/bottomArrow.png')}
                                          defaultValue='Select'
                                          pickerData={[{'a': 'a'}, {'b': 'b'}, {'c': 'c'}, {'d': 'd'}, {'e': 'e'}]}/>
                        </View>
                        <View style={{ flex: 1 }}>
                            <LightText style={{
                                fontSize: 10,
                                color: '#fff',
                                letterSpacing: 2,
                            }}>
                                STARS
                            </LightText>
                            <CustomPicker showLeftImage={true} showRightImage={true}
                                          imageLeft={require('./../assets/starWhite.png')}
                                          imageRight={require('./../assets/bottomArrow.png')}
                                          pickerViewStyles={{width: height1 < 700 ? 70 : 130, marginLeft: 20}}
                                          pickerStyles={{width: height1 < 700 ? 70 : 130}}
                                          underlineWidth={{width: height1 < 700 ? 70 : 130}}
                                          defaultValue='0'
                                          selectedTextStyle={{marginLeft: 40}}
                                          pickerData={[{'1': '1'}, {'2': '2'}, {'3': '3'}, {'4': '4'}, {'5': '5'}]}/>
                        </View>
                    </View>
                    <View style={{ flex: 1 }}>
                        <BoldText style={{fontSize:20, color:'#fff', letterSpacing:2, alignSelf: 'flex-start' }}>Price Range</BoldText>
                        <LightText style={{
                            fontSize: 12,
                            color: 'white',
                            letterSpacing: 1,
                            marginTop: 5
                        }}>{this.state.minimum}$ - {this.state.maximum}$ A NIGHT</LightText>

                    </View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <RangeSlider
                                minValue={0}
                                maxValue={100}
                                tintColor={'#da0f22'}
                                handleBorderWidth={1}
                                handleBorderColor="#454d55"
                                selectedMinimum={20}
                                selectedMaximum={40}
                                style={{ flex: 1, height: 70, padding: 10, backgroundColor: '#ddd' }}
                                onChange={ (data)=>{ console.log(data);} }
                        />
                    </View>
                    <TouchableOpacity
                        style={{
                            alignSelf: 'flex-end',
                            marginTop:20,
                            marginRight:30
                        }}
                        onPress={()=>{this.props.navigation.goBack()}}
                    >
                        <Image
                            style={{
                                alignSelf: 'flex-end',
                                height:100,
                                width:100,
                                borderRadius:50,
                                
                            }}

                            source={require('./../assets/forwardButtonLight.png')}
                        />

                    </TouchableOpacity>

                </View>


            </ImageBackground>
        );
    }
}
