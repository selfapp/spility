import React, {Component} from 'react';
import {Dimensions, Image, ImageBackground, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import StarRating from 'react-native-star-rating';
import Header from './../components/Header';
import { BoldText,
    LightText
 } from '../components/styledTexts';

let height1 = Dimensions.get('window').height;
export default class HotelInfo extends Component {

    constructor(props) {
        super(props);
    };

    render() {
        return (
            <ImageBackground
                style={{flex: 1}}
                source={require('./../assets/background.png')}
            >
                <Header
                    showBackButton={true}
                />
                <ScrollView style={{flex: 1}}>
                    <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                        <BoldText style={{fontSize: 26, color: '#fff', fontWeight: '600'}}>
                            {this.props.navigation.state.params.item.item.name}
                        </BoldText>
                        <View style={{ flexDirection: 'row'}}>
                            <BoldText style={{ color: 'white' }}>
                                299.00$ 
                            </BoldText>
                            <BoldText style={{ color: 'white', marginHorizontal: 10, textDecorationLine: 'line-through' }}>
                                 499.00$
                            </BoldText>
                        </View>
                        <View style={{
                            marginVertical: 15,
                            justifyContent: 'center',
                            height: 26,
                            width: 100,
                            borderRadius: 15,
                            backgroundColor: 'white'
                        }}>
                            <BoldText style={{ textAlign: 'center', color: '#BF68D4', fontSize: 10 }}>
                                2 Km Away
                            </BoldText>
                           
                        </View>
                        <View style={{ marginVertical: 10 }}>
                            <BoldText style={{color: '#fff', fontSize: 12, letterSpacing: 1 }}>
                                ABOUT {this.props.navigation.state.params.item.item.name}
                            </BoldText>

                            <LightText style={{ color: 'white', fontSize: 13, marginVertical: 10, letterSpacing: 1 }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam suscipit ante at nisi facilisis, eu egestas metus scelerisque. Etiam ultricies feugiat ante, nec rhoncus lacus ornare quis. Sed porttitor arcu augue, a mattis mi aliquet nec. Phasellus in pellentesque arcu, non porta ex. Nam vel nisl nec nunc viverra faucibus sed ac libero. Pellentesque eu dignissim leo, a condimentum turpis. Nullam tincidunt mi ac elementum scelerisque.
                            </LightText>
                        </View>
                        <View style={{height: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}/>
                        <View style={{ flexDirection: 'row',alignItems: 'center' }}>
                            <Image
                                source={require('../assets/weather_icon.png')}
                            />
                            <View style={{
                                alignItems: 'center',
                                flexDirection: 'row',
                            }}>
                                <View>
                                    <BoldText style={{color: '#fff', fontSize: 20, fontWeight: '500'}}>
                                        24.0°C
                                    </BoldText>
                                    <BoldText style={{color: '#fff', fontSize: 10, fontWeight: '300'}}>
                                        SUNNY
                                    </BoldText>
                                </View>
                                <View style={{
                                    width: 2,
                                    height: 20,
                                    backgroundColor: 'rgba(255,255,255,0.6)',
                                    marginLeft: 10
                                }}/>
                                <View style={{ marginLeft: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <BoldText style={{color: '#fff', fontSize: 20, fontWeight: '500'}}>
                                     4.6
                                    <BoldText style={{color: 'rgba(255,255,255,0.6)', fontSize: 13 }}>
                                        (1.6k)
                                    </BoldText>
                                 </BoldText>
                                </View>
                               
                                <StarRating
                                    disabled={true}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    starSize={12}
                                    width={50}
                                    rating={4}
                                    fullStarColor={'#EFB824'}
                                />
                               
                            </View>

                                <TouchableOpacity
                                    onPress={() => this.props.navigation.goBack()}
                                    style={{
                                        marginLeft: 15,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Image
                                        source={require('./../assets/dragInverted.png')}
                                    />
                                </TouchableOpacity>
                                {/*</View>*/}
                            </View>
                        </View>

                        <View style={{
                            borderColor: 'rgba(238,238,238,0.5)',
                            backgroundColor: 'rgba(255,255,255,0.2)',
                            borderWidth: 1,
                            borderRadius: 30,
                            marginVertical: 10,
                            padding: 10
                        }}>
                                <View style={{ justifyContent: 'center', marginHorizontal: 10, marginVertical: 20 }}>
                                    <BoldText style={{fontSize: height1 < 700 ? 15 : 25, color: '#fff'}}>
                                        Features
                                    </BoldText>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginVertical: 5, marginHorizontal: 5 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            source={require('./../assets/wifi.png')}
                                        />
                                        <BoldText style={{ color: 'white',textAlign: 'center' }}>WI-FI</BoldText>
                                    </View>
                                    <View style={{height: '30%', width: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}/>

                                    <View style={{ alignItems: 'center' }}>
                                         <Image
                                            source={require('./../assets/parking.png')}
                                         />
                                        <BoldText style={{ color: 'white',textAlign: 'center' }}>Parking</BoldText>
                                    </View>
                                    <View style={{height: '30%', width: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}/>
                                    <View style={{ alignItems: 'center' }}>
                                         <Image
                                            source={require('./../assets/innerBar.png')}
                                         />
                                        <BoldText style={{ color: 'white',textAlign: 'center' }}>{'Inner\nBar'}</BoldText>
                                    </View>

                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginVertical: 15, marginHorizontal: 5 }}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            source={require('./../assets/swimmingPool.png')}
                                        />
                                        <BoldText style={{ color: 'white', textAlign: 'center' }}>{'Swimming\nPool'}</BoldText>
                                    </View>
                                    <View style={{height: '30%', width: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}/>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            source={require('./../assets/cafe_restaurant_icon.png')}
                                        />
                                        <BoldText style={{ color: 'white', textAlign: 'center' }}>{'Cafe\n Restaurant'}</BoldText>
                                    </View>
                                    <View style={{height: '30%', width: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}/>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            source={require('./../assets/gym.png')}
                                        />
                                        <BoldText style={{ color: 'white', textAlign: 'center' }}>Gym</BoldText>
                                    </View>
                                </View>
                        </View>

                    </View>
                    <TouchableOpacity
                        style={{
                            width: 300,
                            height: 50,
                            marginBottom: 40,
                            marginTop: 30,
                            backgroundColor: '#fff',
                            alignSelf: 'center',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 25
                        }}
                    >
                        <BoldText style={{color: '#4D55AA'}}>Book a Room</BoldText>
                    </TouchableOpacity>
                </ScrollView>


            </ImageBackground>
        );
    }
}
