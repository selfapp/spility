//https://rest-hotels-api.herokuapp.com/v1/hotels api for hotels


import React, {Component} from "react";
import {Dimensions, FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Reactotron from 'reactotron-react-native';
import Header from "./../components/Header";
import LinearGradient from 'react-native-linear-gradient';
import StarRating from 'react-native-star-rating';
import { BoldText,
    LightText
 } from '../components/styledTexts';

let height1 = Dimensions.get('window').height;
// let width1 = Dimensions.get('window').width;
export default class SearchResults extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchResults: null
        }

    }

    async componentDidMount() {
        await fetch(
            'https://rest-hotels-api.herokuapp.com/v1/hotels'
            , {
                method: 'GET'
            }
        )
            .then(response => response.json())
            // .then(responseJson => Reactotron.log(responseJson))
            .then(responseJson => this.setState({searchResults: responseJson}))
            .catch(errors => Reactotron.log(errors));
    }

    render() {

        return (
            <View style={{flex: 1, backgroundColor: '#4C4C4C'}}>
                <Header
                    showBackButton={true}
                    title={"Hotel"}
                    headerContainerStyles={{backgroundColor: '#fff', marginTop: 0}}
                    backButtonBlack={true}
                    backButtonBlackStyles={{justifyContent: 'center', height: '100%'}}
                    titleContainer={{alignSelf: 'center'}}
                    titleStyle={{color: '#000'}}
                    showFilter={true}
                />
                <View style={{flex: 1}}>
                    <FlatList
                        data={this.state.searchResults}
                        extraData={this.state.searchResults}
                        keyExtractor={(item, index) => index.toString()}
                        style={{flex: 1}}
                        renderItem={
                            (item) =>
                                <TouchableOpacity style={{ flex: 1 }}
                                    onPress={() => this.props.navigation.navigate('hotelNavigationOptions', {item: item})}
                                >
                                    <ImageBackground
                                        style={{ flex: 1 }}
                                        source={{ uri: item.id > 1 ? item.item.images[0] : item.item.images[0] }}
                                    >
                                         <LinearGradient
                                            colors={['rgba(255,255,255,0)','rgba(0,0,0,0.5)', 'rgba(0,0,0,1)']}
                                            style={{flex:1}}
                                            start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }}
                                        >
                                        
                                        <View style={{ flex: 1, marginVertical: 60 }}/>
                                            <View
                                                style={{ flex: 1,
                                                         marginHorizontal: 25
                                                }}
                                            >
                                                <BoldText
                                                    style={{
                                                        fontSize: height1 < 760 ? 20 : 25,
                                                        color: '#fff',
                                                        textShadowColor: 'black',
                                                        textShadowOffset: { height: 0.5, width: 0.5 },
                                                        textShadowRadius: 1

                                                    }}
                                                >
                                                    {item.item.name}
                                                </BoldText>
                                                <View style={{ flexDirection: 'row'}}>
                                                     <BoldText style={{ color: 'white' }}>
                                                        299.00$ 
                                                     </BoldText>
                                                    <BoldText style={{ color: 'white', marginHorizontal: 10, textDecorationLine: 'line-through' }}>
                                                        499.00$
                                                    </BoldText>
                                                </View>

                                                <View
                                                    style={{
                                                        height: 2,
                                                        backgroundColor: 'white',
                                                        opacity: .3,
                                                        marginTop: 10
                                                    }}
                                                />
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        flex: 1,
                                                        justifyContent: 'space-around',
                                                        }}
                                                 >
                                                    <Image
                                                        style={{ height: 60, width: 60 }}
                                                        source={require('../assets/weather_icon.png')}
                                                    />
                                                    <View
                                                        style={{
                                                            flexDirection: 'row',
                                                            justifyContent: 'space-around'
                                                        }}
                                                    >
                                                        <View>
                                                            <BoldText
                                                                style={{
                                                                    color: '#fff',
                                                                    fontSize: 15,
                                                                    fontWeight: '500'
                                                                }}
                                                            >
                                                                24.0°C
                                                            </BoldText>
                                                            <LightText
                                                                style={{color: 'white', fontSize: 10 }}>
                                                                SUNNY
                                                            </LightText>
                                                        </View>
                                                        <View style={{
                                                            width: 2,
                                                            backgroundColor: '#fff',
                                                            opacity: .5,
                                                            marginLeft: 10
                                                        }}/>
                                                        <View style={{ marginLeft: 10 }}>
                                                         <View style={{ flexDirection: 'row' }}>
                                                            <BoldText style={{color: '#fff', fontSize: 20, fontWeight: '500'}}>
                                                              4.6
                                                            <BoldText style={{color: 'rgba(255,255,255,0.6)', fontSize: 13 }}>
                                                                (1.6k)
                                                            </BoldText>
                                                            </BoldText>
                                                        </View>
                                                        <StarRating
                                                            disabled={true}
                                                            emptyStar={'ios-star-outline'}
                                                            fullStar={'ios-star'}
                                                            halfStar={'ios-star-half'}
                                                            iconSet={'Ionicons'}
                                                            maxStars={5}
                                                            starSize={12}
                                                            width={50}
                                                            rating={4}
                                                            fullStarColor={'#EFB824'}
                                                        />
                               
                                                    </View>
                                                        <View style={{
                                                            marginLeft: 20,
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            height: 16,
                                                            width: 80,
                                                            borderRadius: 9,
                                                            backgroundColor: '#fff',
                                                            flexDirection: 'row'
                                                        }}>
                                                            <Image
                                                                source={require('./../assets/count.png')}
                                                            />
                                                            <BoldText style={{
                                                                marginLeft: 10,
                                                                color: '#000',
                                                                fontSize: 10,
                                                                fontWeight: '200'
                                                            }}>
                                                                5 Times
                                                            </BoldText>

                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </LinearGradient>
                                    </ImageBackground>
                                </TouchableOpacity>
                        }
                    />
                </View>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    linearGradient: {
        flex: 1
    }
});
