import React, {Component} from 'react';
import {
    View,
    FlatList,
    Image,
    Text,
    TouchableOpacity,
    AsyncStorage,
} from 'react-native';
import {connect} from 'react-redux';
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";
import Loader from '../components/Loader';
import {BoldText, LightText} from "../components/styledTexts";
import {chatOpened, chatOpening, loadLast10People} from "../store/actions/chat";
import API from "../components/API";
import Header from '../components/Header';

class Chats extends Component {
    componentDidMount() {
        this.props.loadLast10Users();
    }

    gotoChat(item, cb = () => {}) {
        var channel = '';
        if (item.phone_number > this.props.phone_number) {
            channel = this.props.phone_number + '_' + item.phone_number;
        } else {
            channel = item.phone_number + '_' + this.props.phone_number
        }
        try {
            ChatEngineProvider.getChatRoomModel()
                .connect(channel)
                .then(() => {
                    cb();
                    this.props.navigation.navigate("ChatStart", {
                        name: item.first_name,
                        imageUrl: `${API.baseURL}${this.props.small_profile_image}`
                    });
                });
        } catch (error) {
            console.log("error are ===" + error)
        }
    }

    gotoChatList(item) {
        var channel = '';
        if (item.phone_number > this.props.phone_number) {
            channel = this.props.phone_number + '_' + item.phone_number;
        } else {
            channel = item.phone_number + '_' + this.props.phone_number
        }
        try {
            ChatEngineProvider.getChatRoomModel()
                .connect(channel)
                .then(() => {
                    setTimeout(() => {
                        this.props.navigation.navigate("UserList", {
                            name: item.first_name,
                            imageUrl: `${API.baseURL}${item.small_profile_image}`
                        });
                        // this.props.navigation.navigate("ChatStart", {name:item.first_name, imageUrl:`${API.baseURL}${item.small_profile_image}`});
                    }, 1000);
                });
        } catch (error) {
            console.log("error are ===" + error)
        }
    }


    render() {
        return <View style={{flex: 1 }}>
            <Header titleStyle={{fontSize: 22}} title={'Chat'} headerContainerStyles={{backgroundColor: 'rgba(69, 41, 176, 1)'}}/>
            <View style={{backgroundColor: '#FFFFFF', paddingVertical: 20}}>
                <FlatList
                    data={this.props.last10Users}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) => {
                        return <TouchableOpacity
                            style={{alignItems: 'center'}}
                            onPress={() => {
                                this.props.chatOpening();
                                this.gotoChat(item, this.props.chatOpened)
                            }}>
                            <View style={{
                                borderWidth: 1,
                                borderColor: 'gray',
                                height: 51,
                                width: 51,
                                borderRadius: 51 / 2,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                                {
                                    item.thumb_profile_image ?
                                        <Image
                                            source={{uri: `${API.baseURL}${item.thumb_profile_image}`}}
                                            style={{height: 46, width: 46, borderRadius: 23}}
                                            resizeMode={'contain'}
                                        />
                                        :
                                        <Image
                                            style={{
                                                width: 46,
                                                height: 46,
                                                borderRadius: 23,
                                                alignSelf: 'center',
                                                backgroundColor: 'grey'
                                            }}
                                            source={require('../assets/loginSplttyLogo.png')}
                                            resizeMode={'contain'}
                                        />
                                }
                            </View>
                            <Text>{item.first_name}</Text>
                        </TouchableOpacity>
                    }}
                    style={{marginHorizontal: 20, height: 71}}
                    horizontal={true}
                    ItemSeparatorComponent={() => {
                        return <View style={{width: 10}}/>
                    }}
                />
            </View>
            <View style={{flex: 1}}>
                {
                    this.props.uuid ?
                        <FlatList
                            data={this.props.last10Users}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item}) => {
                                return <TouchableOpacity
                                    style={{paddingVertical: 20, flexDirection: 'row', paddingHorizontal: 20}}
                                    onPress={() => {
                                        this.props.chatOpening();
                                        this.gotoChat(item, this.props.chatOpened);
                                    }}
                                >
                                    {item.thumb_profile_image ?
                                        <Image
                                            source={{uri: `${API.baseURL}${item.thumb_profile_image}`}}
                                            style={{height: 50, width: 50, borderRadius: 25}}
                                            resizeMode={'contain'}
                                        />
                                        :
                                        <Image
                                            style={{
                                                width: 50,
                                                height: 50,
                                                borderRadius: 25,
                                                alignSelf: 'center',
                                                backgroundColor: 'grey'
                                            }}
                                            source={require('../assets/loginSplttyLogo.png')}
                                            resizeMode={'contain'}
                                        />
                                    }
                                    <View style={{flex: 1, padding: 10, justifyContent: 'center'}}>
                                        <BoldText>{item.first_name} {item.last_name}</BoldText>
                                    </View>
                                    <View style={{justifyContent: 'center'}}>
                                        <LightText>9:41 am</LightText>
                                        <LightText/>
                                    </View>
                                </TouchableOpacity>
                            }}
                            ItemSeparatorComponent={() => <View
                                style={{height: 1, backgroundColor: 'rgb(234, 235, 238)'}}/>}
                            style={{flex: 1}}
                        />
                        :
                        <BoldText style={{color: 'black', fontSize: 20, textAlign: 'center'}}>Visit the profile page to
                            login</BoldText>
                }
            </View>
            <Loader loading={this.props.loading}/>
        </View>;
    }
}

const mapStateToProps = state => {
    return {
        last10Users: state.chats.last10Users,
        loading: state.chats.loading,
        uuid: state.user.uuid,
        phone_number: state.user.phone_number,
        small_profile_image: state.user.small_profile_image
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadLast10Users: () => loadLast10People(dispatch),
        chatOpening: () => dispatch(chatOpening()),
        chatOpened: () => dispatch(chatOpened())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Chats);
