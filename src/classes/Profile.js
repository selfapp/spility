import React, {Component} from 'react';
import {
    ImageBackground,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    FlatList,
    Modal,
    AsyncStorage,
    Linking
} from 'react-native';
import {connect} from 'react-redux';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { StackActions, NavigationActions } from 'react-navigation';
import ImageViewer from 'react-native-image-zoom-viewer';
import API from '../components/API';
import Header from '../components/Header';
import { BoldText, LightText } from '../components/styledTexts';
import {logout, updateProfileFromServer, updateUserLocation} from "../store/actions/user";
import Geolocation from 'react-native-geolocation-service';
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";

let interests = [
    {
        name: 'badminton',
        image: require('../assets/interests/badmintion.png')
    },
    {
        name: 'baseball',
        image: require('../assets/interests/baseball.png')
    },
    {
        name: 'basketball',
        image: require('../assets/interests/basketball.png')
    },
    {
        name: 'blogging',
        image: require('../assets/interests/blogging.png')
    },
    {
        name: 'cricket',
        image: require('../assets/interests/cricket.png')
    },
    {
        name: 'dance',
        image: require('../assets/interests/dance.png')
    },
    {
        name: 'fashion',
        image: require('../assets/interests/fashion.png')
    },
    {
        name: 'food',
        image: require('../assets/interests/food_click.png')
    },
    {
        name: 'football',
        image: require('../assets/interests/football.png')
    },
    {
        name: 'hiking',
        image: require('../assets/interests/hiking_click.png')
    },
    {
        name: 'hockey',
        image: require('../assets/interests/hockey.png')
    },
    {
        name: 'movies',
        image: require('../assets/interests/movies.png')
    },
    {
        name: 'museum',
        image: require('../assets/interests/museum_click.png')
    },
    {
        name: 'music',
        image: require('../assets/interests/music.png')
    },
    {
        name: 'painting',
        image: require('../assets/interests/painting_click.png')
    },
    {
        name: 'photography',
        image: require('../assets/interests/photography.png')
    },
    {
        name: 'scuba',
        image: require('../assets/interests/scuba_click.png')
    },
    {
        name: 'tennis',
        image: require('../assets/interests/tennis.png')
    },
    {
        name: 'travel',
        image: require('../assets/interests/travel.png')
    },
    {
        name: 'volleyball',
        image: require('../assets/interests/volleyball.png')
    }
];

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            profile_image_modal: false,
            passport_modal: false
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('accessToken')
            .then((token) => {
                if(token == null) {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'loginNavigationOptions' })]
                    });
                    this.props.navigation.dispatch(resetAction);
                } else {
                    AsyncStorage.getItem('user')
                        .then((user) => {
                            user = JSON.parse(user);
                            if(user.first_name == null) {
                                const resetAction = StackActions.reset({
                                    index: 0,
                                    actions: [
                                        NavigationActions.navigate({
                                            routeName: 'CompleteProfile'
                                        })
                                    ],
                                    key: null
                                });
                                this.props.navigation.dispatch(resetAction);
                            } else {
                                this.props.updateProfileFromServer(user);
                                Geolocation.getCurrentPosition(
                                    position => {
                                        this.props.updateUserLocation(position.coords.latitude, position.coords.longitude)
                                    },
                                    error => {},
                                    {
                                        showLocationDialog: true
                                    }
                                );

                                ChatEngineProvider.connect(
                                    this.props.phone_number,
                                    this.props.phone_number
                                ).then(async () => {
                                    try {
                                        AsyncStorage.setItem(
                                            ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY,
                                            JSON.stringify({
                                                username: "rajeev",
                                                name: "rajeev"
                                            })
                                        );
                                    } catch (error) {
                                        console.log("error during chat initailise " + error);
                                    }
                                });
                            }
                        });
                }
            });
    }

    render() {
        return <ImageBackground style={{flex: 1}} source={require('./../assets/background.png')}>
            <Header title={'Profile'} />
            <ScrollView style={{ flex: 1 }}>
                {/*Top Section*/}
                <View style={{ flexDirection: 'row', margin: 20 }}>
                    <View style={{ height: 85, width: 85, borderRadius: 40, borderWidth: 2, borderColor: 'white', alignItems: 'center', justifyContent: 'center', marginVertical: 10 }}>
                        {
                            this.props.profile_image !== null && this.props.profile_image.length > 0
                                ?
                                <TouchableOpacity
                                    onPress={() => this.setState({ profile_image_modal: true })}
                                >
                                    <Image
                                        source={{ uri: `${API.baseURL}${this.props.thumb_profile_image}` }}
                                        style={{ height: 70, width: 70, borderRadius: 35 }}
                                        resizeMode={'cover'}
                                    />
                                </TouchableOpacity>
                                :
                                <Image
                                    source={require('../assets/loginSplttyLogo.png')}
                                    style={{ height: 70, width: 70, borderRadius: 35 }}
                                    resizeMode={'contain'}
                                />
                        }
                    </View>

                    <View style={{ flex: 1, justifyContent: 'space-around', marginLeft: 20 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <BoldText style={{ color: 'white' }}>43</BoldText>
                                <LightText style={{ color: 'white', fontSize: 12 }}>Trips</LightText>
                            </View>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <BoldText style={{ color: 'white' }}>43k</BoldText>
                                <LightText style={{ color: 'white', fontSize: 12 }}>Favourites</LightText>
                            </View>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <BoldText style={{ color: 'white' }}>3k</BoldText>
                                <LightText style={{ color: 'white', fontSize: 12 }}>Points</LightText>
                            </View>
                        </View>

                        <TouchableOpacity style={{ borderWidth: 1, borderColor: 'white', borderRadius: 5, paddingVertical: 7, alignItems: 'center' }}
                                          onPress={() => this.props.navigation.navigate('EditProfile')}
                        >
                            <BoldText style={{ color: 'white', fontSize: 18 }}>Edit Profile</BoldText>
                        </TouchableOpacity>
                    </View>
                </View>

                {/*Top Information Section*/}
                <View style={{ marginHorizontal: 20 }}>
                    <BoldText style={{ color: 'white', fontSize: 20 }}>{this.props.first_name} {this.props.last_name}</BoldText>
                    <LightText style={{ color: 'white', fontSize: 16, marginTop: 5 }}>{this.props.description}</LightText>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                        <Ionicon name={'ios-pin'} size={20} color={'#FFFFFF'} />
                        <BoldText style={{ color: 'white', fontSize: 16, marginLeft: 10 }}>{this.props.current_location}</BoldText>
                    </View>
                </View>

                {/*Line*/}
                <View style={{ height: 1, backgroundColor: '#CBCBCB', marginVertical: 10 }} />

                {/*Profile Details*/}
                <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                    {/*Header*/}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Ionicon name={'ios-menu'} size={20} color={'#FFFFFF'} />
                        <BoldText style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>Profile Summary</BoldText>
                    </View>

                    {/*Details*/}
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', flex: 0.5, alignItems: 'center' }}>
                            <Ionicon name={'ios-mail'} size={20} color={'#CBCBCB'} />
                            <LightText style={{ color: '#CBCBCB', marginLeft: 10 }}>Email:</LightText>
                        </View>
                        <LightText style={{ color: 'white', flex: 1 }}>{this.props.email}</LightText>
                    </View>
                    <View style={{ height: 1, backgroundColor: '#CBCBCB', marginVertical: 10 }} />

                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 0.5, alignItems: 'center' }}>
                            <Ionicon name={'ios-call'} size={20} color={'#CBCBCB'} />
                            <LightText style={{ color: '#CBCBCB', marginLeft: 10 }}>Phone no.:</LightText>
                        </View>
                        <LightText style={{ color: 'white', flex: 1 }}>{this.props.phone_number}</LightText>
                    </View>
                    <View style={{ height: 1, backgroundColor: '#CBCBCB', marginVertical: 10 }} />

                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 0.5, alignItems: 'center' }}>
                            <Ionicon name={'ios-home'} size={20} color={'#CBCBCB'} />
                            <LightText style={{ color: '#CBCBCB', marginLeft: 10 }}>Address:</LightText>
                        </View>
                        <LightText style={{ color: 'white', flex: 1 }}>{this.props.city}, {this.props.state}, {this.props.country}</LightText>
                    </View>
                    <View style={{ height: 1, backgroundColor: '#CBCBCB', marginVertical: 10 }} />

                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', flex: 0.5, alignItems: 'center' }}>
                            <Ionicon name={'ios-transgender'} size={20} color={'#CBCBCB'} />
                            <LightText style={{ color: '#CBCBCB', marginLeft: 10 }}>Gender:</LightText>
                        </View>
                        <LightText style={{ color: 'white', flex: 1 }}>{this.props.gender}</LightText>
                    </View>
                    <View style={{ height: 1, backgroundColor: '#CBCBCB', marginVertical: 10 }} />
                </View>

                {/*Line*/}
                <View style={{ height: 1, backgroundColor: '#CBCBCB', marginBottom: 10 }} />

                {/*My Interests*/}
                <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                    {/*Header*/}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Ionicon name={'ios-heart-empty'} size={15} color={'#FFFFFF'} />
                        <BoldText style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>My Interests</BoldText>
                    </View>
                    <FlatList
                        data={interests}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item}) => {
                            if(this.props.interests.indexOf(item.name) > -1) {
                                return <View
                                    style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}
                                >
                                    <View style={{ height: 45, width: 45, borderWidth: 1, borderColor: 'white', borderRadius: 22.5, alignItems: 'center', justifyContent: 'center', backgroundColor: this.props.interests.indexOf(item.name) > -1 ? '#FFFFFF' : 'rgba(0, 0, 0, 0)' }}>
                                        <Image
                                            source={item.image}
                                            style={{ height: 30, width: 30 }}
                                            resizeMode={'contain'}
                                        />
                                    </View>
                                    <LightText style={{ textAlign: 'center', color: 'white', marginTop: 5, fontSize: 12 }}>{item.name}</LightText>
                                </View>
                            } else {
                                return null;
                            }
                        }}
                        horizontal={true}
                        style={{ marginTop: 10 }}
                    />
                </View>

                {/*Line*/}
                <View style={{ height: 1, backgroundColor: '#CBCBCB', marginBottom: 10 }} />

                {/*Verification Entities*/}
                <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                    {/*Header*/}
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            source={require('../assets/profile/stack.png')}
                            style={{ height: 15, width: 15 }}
                            resizeMode={'contain'}
                        />
                        <BoldText style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>Verification Entities</BoldText>
                    </View>

                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                        <Image
                            source={require('../assets/profile/passport.png')}
                            style={{height: 25, width: 25}}
                            resizeMode={'contain'}
                        />
                        <LightText style={{color: '#CBCBCB', marginLeft: 20}}>Passport</LightText>

                        <View style={{flex: 1, alignItems: 'center'}}>
                            {this.props.passport !== null ?
                                <TouchableOpacity
                                    onPress={() => this.setState({ passport_modal: true })}
                                >
                                    <Image
                                        style={{height: 40, width: 40, borderRadius: (40 / 2)}}
                                        resizeMode='cover'
                                        source={{uri: `${API.baseURL}${this.props.passport}`}}
                                    />
                                </TouchableOpacity>
                                :
                                null
                            }
                        </View>

                    </View>
                    <View style={{height: 1, backgroundColor: '#CBCBCB', marginVertical: 10}}/>

                    {
                        (this.props.facebook_username) ?
                            <TouchableOpacity
                                style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20, borderRadius: 5, borderWidth: 1, borderColor: '#FFF', paddingVertical: 10, justifyContent: 'center'}}
                                onPress={() => Linking.openURL(`https://fb.me/${this.props.facebook_username}`)}
                            >
                                <Image
                                    source={require('../assets/profile/facebook.png')}
                                    style={{height: 25, width: 25}}
                                    resizeMode={'contain'}
                                />

                                <BoldText style={{ color: 'white', marginLeft: 10 }}>Tap here to visit my Facebook</BoldText>
                            </TouchableOpacity>
                            :
                            null
                    }

                    {
                        this.props.instagram_username ?
                            <TouchableOpacity
                                style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10, paddingVertical: 10, borderRadius: 5, borderWidth: 1, borderColor: '#FFF', justifyContent: 'center'}}
                                onPress={() => Linking.openURL(`https://instagram.com/${this.props.instagram_username}`)}
                            >
                                <Image
                                    source={require('../assets/profile/instagram.png')}
                                    style={{height: 25, width: 25}}
                                    resizeMode={'contain'}
                                />

                                <BoldText style={{ color: 'white', marginLeft: 10 }}>Tap here to visit my Instagram</BoldText>
                            </TouchableOpacity>
                            :
                            null
                    }

                    {
                        this.props.linkedin_username ?
                            <TouchableOpacity
                                style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20, borderRadius: 5, borderWidth: 1, borderColor: '#FFF', justifyContent: 'center', paddingVertical: 10}}
                                onPress={() => Linking.openURL(`https://www.linkedin.com/${this.props.linkedin_username}`)}
                            >
                                <Image
                                    source={require('../assets/profile/linkedin.png')}
                                    style={{height: 25, width: 25}}
                                    resizeMode={'contain'}
                                />

                                <BoldText  style={{ color: 'white', marginLeft: 10 }}>Tap here to visit my Linkedin</BoldText>
                            </TouchableOpacity>
                            :
                            null
                    }
                </View>

                <TouchableOpacity
                    style={{ padding: 10, borderWidth: 1, borderColor: 'white', borderRadius: 5, alignSelf: 'center', paddingHorizontal: 40, marginBottom: 20 }}
                    onPress={() => this.props.logout(this.props.navigation)}
                >
                    <LightText style={{ textAlign: 'center', color: 'white' }}>Logout</LightText>
                </TouchableOpacity>
            </ScrollView>
            <Modal
                visible={this.state.profile_image_modal}
                transparent={false}
                style={{ flex: 1 }}
            >
                <View style={{ flexDirection: 'row', zIndex: 100 }}>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity style={{ position: 'relative', top: 60, right: 20 }} onPress={() => this.setState({ profile_image_modal: false })}>
                        <Ionicon name={'ios-close'} size={50} color={'#FFFFFF'} />
                    </TouchableOpacity>
                </View>
                <ImageViewer
                    imageUrls={[{ url: `${API.baseURL}${this.props.small_profile_image}` }]}
                    onCancel={() => {
                        this.setState({
                            profile_picture_modal: false
                        });
                    }}
                    renderIndicator={() => null}
                />
            </Modal>
            <Modal
                visible={this.state.passport_modal}
                transparent={false}
                style={{ flex: 1 }}
            >
                <View style={{ flexDirection: 'row', zIndex: 100 }}>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity style={{ position: 'relative', top: 60, right: 20 }} onPress={() => this.setState({ passport_modal: false })}>
                        <Ionicon name={'ios-close'} size={50} color={'#FFFFFF'} />
                    </TouchableOpacity>
                </View>
                <ImageViewer
                    imageUrls={[{ url: `${API.baseURL}${this.props.small_passport}` }]}
                    onCancel={() => {
                        this.setState({
                            passport_modal: false
                        });
                    }}
                    renderIndicator={() => null}
                />
            </Modal>
        </ImageBackground>;
    }
}

const mapStateToProps = state => {
    return {
        email: state.user.email,
        first_name: state.user.first_name,
        last_name: state.user.last_name,
        city: state.user.city,
        state: state.user.state,
        country: state.user.country,
        description: state.user.description,
        gender: state.user.gender,
        loading: state.user.loading,
        phone_number: state.user.phone_number,
        profile_image: state.user.profile_image,
        passport: state.user.passport,
        linkedin_username: state.user.linkedin_username,
        facebook_username: state.user.facebook_username,
        instagram_username: state.user.instagram_username,
        interests: state.user.interests,
        current_location: state.user.current_location,
        thumb_profile_image: state.user.thumb_profile_image,
        thumb_passport: state.user.thumb_passport,
        small_profile_image: state.user.small_profile_image,
        small_passport: state.user.small_passport
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateProfileFromServer: data => dispatch(updateProfileFromServer(data)),
        updateUserLocation: (lat, lng) => updateUserLocation(lat, lng, dispatch),
        logout: (navigation) => logout(navigation, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
