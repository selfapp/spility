import React, {Component} from 'react';
import {
    View,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    SafeAreaView,
} from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import {connect} from 'react-redux';
import api from '../components/API';
import Loader from '../components/Loader';
import {BoldText, LightText} from '../components/styledTexts';
import {verifyOTP} from "../store/actions/user";

let {height, width} = Dimensions.get('window');

class VerificationCodeCheck extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            otp: ''
        }
    }

    async reSendOTP() {
        this.setState({loader: true});
        let body = {
            "user": {
                "phone_number": this.props.navigation.state.params.phoneNumber,
                "country_code": '+' + this.props.navigation.state.params.country_code
            }
        };
        try {
            let response = await api.request('request_sms_verification_code', 'post', body);
            this.setState({loader: false});
            if (response.status === 200) {
                await response.json().then((res) => {
                    alert(res.message);
                });
            } else {
                await response.json().then((res) => {
                    alert(res.message);
                });
            }
        } catch (error) {
            this.setState({loader: false});
            alert(error);
        }
    }

    render() {
        return (
            <ImageBackground
                style={{flex: 1}}
                source={require("./../assets/background.png")}
            >
                <SafeAreaView style={{flex: 1}}>
                    <KeyboardAvoidingView contentContainerStyle={{flex: 1}} style={{height: height, width: width}}
                                          behavior='position' enabled keyboardVerticalOffset={-250}>
                        <View style={{flex: 1, marginHorizontal: 30, paddingVertical: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableOpacity
                                    style={{flex: 0.5}}
                                    onPress={() => {
                                        this.props.navigation.goBack()
                                    }}
                                >
                                    <Image style={{height: 30, width: 30,}}
                                           resizeMode='contain'
                                           source={require('./../assets/back-button.png')}
                                    />
                                </TouchableOpacity>
                                <BoldText style={{color: 'white', fontSize: 20}}>Sign up</BoldText>
                                <View style={{flex: 0.5}}/>
                            </View>
                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <Image
                                    style={{height: 150, width: 150, alignSelf: 'center'}}
                                    resizeMode='contain'
                                    source={require('./../assets/loginSplttyLogo.png')}
                                />
                            </View>
                            <BoldText style={{textAlign: 'center', color: 'rgba(245,245,245, 0.7)'}}>We have sent you an
                                access code on
                                +{this.props.navigation.state.params.country_code} {this.props.navigation.state.params.phoneNumber} via
                                SMS for Mobile number verification</BoldText>
                            <View style={{flex: 0.7, alignItems: 'center', justifyContent: 'center'}}>
                                <LightText style={{ color: 'red', textAlign: 'center', marginTop: 10 }}>{this.props.errors.otp ? this.props.errors.otp[0] : null}</LightText>
                                <CodeInput
                                    ref="codeInputRef1"
                                    secureTextEntry
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    codeLength={4}
                                    className={'border-b'}
                                    space={5}
                                    size={40}
                                    fontFamily={'Nexa-Bold'}
                                    inputPosition='left'
                                    onFulfill={(code) => {
                                        this.setState({ otp: code });
                                        this.props.verifyOTP(code, this.props.navigation.state.params.country_code, this.props.navigation.state.params.phoneNumber, this.props.navigation)
                                    }}
                                />
                            </View>


                            <TouchableOpacity
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 45,
                                    borderRadius: 22.5,
                                    backgroundColor: 'white'
                                }}
                                onPress={() => this.props.verifyOTP(this.state.otp, this.props.navigation.state.params.country_code, this.props.navigation.state.params.phoneNumber, this.props.navigation)}
                            >
                                <BoldText style={{color: '#5F5D70'}}>Continue</BoldText>
                            </TouchableOpacity>

                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <View style={{height: 2, backgroundColor: 'rgba(255,255,255, 0.24)'}}/>
                                <View style={{flexDirection: 'row', marginTop: 5}}>
                                    <View style={{flex: 0.5}}/>
                                    <TouchableOpacity style={{}}
                                                      onPress={this.reSendOTP.bind(this)}
                                    >
                                        <BoldText style={{
                                            color: 'rgba(245,245,245, 0.7)',
                                            letterSpacing: 1,
                                            textAlign: 'center'
                                        }}>RESEND CODE</BoldText>
                                    </TouchableOpacity>
                                    <View style={{flex: 0.5}}/>
                                </View>
                            </View>
                            <View style={{flex: 0.5}}/>
                        </View>
                        <Loader loading={this.props.loading} />
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.user.loading,
        errors: state.user.errors
    }
};

const mapDispatchToProps = dispatch => {
    return {
        verifyOTP: (code, country_code, phone_number, navigation) => verifyOTP(code, country_code, phone_number, dispatch, navigation)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerificationCodeCheck);
