import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    SafeAreaView,
    Image,
    ImageBackground,
    TouchableOpacity,
    View,
    AsyncStorage,
    FlatList,
    ActionSheetIOS
} from 'react-native';
import {Hoshi} from 'react-native-textinput-effects';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Ionicon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import Header from "../components/Header";
import {BoldText, LightText} from '../components/styledTexts';
import {
    typeFirstName,
    typeLastName,
    typeDescription,
    typeEmail,
    typeCity,
    typeState,
    typeCountry,
    selectGender,
    typeFacebook,
    typeInstagram,
    typeLinkedin,
    updateProfileFromServer,
    updateProfile,
    updateInterests
} from '../store/actions/user';
import Loader from '../components/Loader';
import API from "../components/API";


let interests = [
    {
        name: 'badminton',
        image: require('../assets/interests/badmintion.png')
    },
    {
        name: 'baseball',
        image: require('../assets/interests/baseball.png')
    },
    {
        name: 'basketball',
        image: require('../assets/interests/basketball.png')
    },
    {
        name: 'blogging',
        image: require('../assets/interests/blogging.png')
    },
    {
        name: 'cricket',
        image: require('../assets/interests/cricket.png')
    },
    {
        name: 'dance',
        image: require('../assets/interests/dance.png')
    },
    {
        name: 'fashion',
        image: require('../assets/interests/fashion.png')
    },
    {
        name: 'food',
        image: require('../assets/interests/food_click.png')
    },
    {
        name: 'football',
        image: require('../assets/interests/football.png')
    },
    {
        name: 'hiking',
        image: require('../assets/interests/hiking_click.png')
    },
    {
        name: 'hockey',
        image: require('../assets/interests/hockey.png')
    },
    {
        name: 'movies',
        image: require('../assets/interests/movies.png')
    },
    {
        name: 'museum',
        image: require('../assets/interests/museum_click.png')
    },
    {
        name: 'music',
        image: require('../assets/interests/music.png')
    },
    {
        name: 'painting',
        image: require('../assets/interests/painting_click.png')
    },
    {
        name: 'photography',
        image: require('../assets/interests/photography.png')
    },
    {
        name: 'scuba',
        image: require('../assets/interests/scuba_click.png')
    },
    {
        name: 'tennis',
        image: require('../assets/interests/tennis.png')
    },
    {
        name: 'travel',
        image: require('../assets/interests/travel.png')
    },
    {
        name: 'volleyball',
        image: require('../assets/interests/volleyball.png')
    }
];

class CompleteProfile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            profile_picture: null,
            passport_picture: null,
            uuid: null
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((user) => {
            user = JSON.parse(user);
            this.setState({
                uuid: user.uuid
            });

            API.getUser()
                .then(res => res.json())
                .then(jsonRes => this.props.updateProfileFromServer(jsonRes.user));
        });
    }

    openGalleryForProfilePic() {
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: ['Take photo', 'Choose from Library', 'Cancel'],
                cancelButtonIndex: 2
            },
            buttonIndex => {
                if(buttonIndex === 1) {
                    ImagePicker.openPicker({
                        width: 1000,
                        height: 1000,
                        cropping: true,
                        includeBase64: true,
                        cropperCircleOverlay: true,
                        mediaType: 'photo'
                    })
                        .then(image => {
                            this.setState({
                                profile_picture: `data:${image.mime};base64,${image.data}`,
                            });
                        });
                } else if(buttonIndex === 0) {
                    ImagePicker.openCamera({
                        width: 1000,
                        height: 1000,
                        cropping: true,
                        includeBase64: true,
                        cropperCircleOverlay: true,
                        mediaType: 'photo'
                    })
                        .then(image => {
                            this.setState({
                                profile_picture: `data:${image.mime};base64,${image.data}`,
                            });
                        });
                }
            }
        );
    }

    openGalleryForPassport() {
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: ['Take photo', 'Choose from Library', 'Cancel'],
                cancelButtonIndex: 2
            },
            buttonIndex => {
                if(buttonIndex === 1) {
                    ImagePicker.openPicker({
                        width: 1000,
                        height: 1000,
                        cropping: true,
                        includeBase64: true,
                        mediaType: 'photo'
                    })
                        .then(image => {
                            this.setState({
                                passport_picture: `data:${image.mime};base64,${image.data}`,
                            });
                        });
                } else if(buttonIndex === 0) {
                    ImagePicker.openCamera({
                        width: 1000,
                        height: 1000,
                        cropping: true,
                        includeBase64: true,
                        mediaType: 'photo'
                    })
                        .then(image => {
                            this.setState({
                                passport_picture: `data:${image.mime};base64,${image.data}`,
                            });
                        });
                }
            }
        );
    }

    render() {
        return (
            <ImageBackground style={{flex: 1}} source={require('./../assets/background.png')}>
                <Header title={"Complete Profile"} showFilter={false}/>

                <SafeAreaView style={{flex: 1}}>
                    <KeyboardAwareScrollView
                        // behavior={Platform.OS === "ios" ? "padding" : null}
                        style={{flex: 1}}
                        // keyboardVerticalOffset={Platform.select({ios: -10, android: 20})}
                    >
                        <TouchableOpacity
                            style={{
                                width: 120,
                                height: 120,
                                marginTop: 40,
                                borderRadius: 60,
                                borderWidth: 2,
                                justifyContent: 'center',
                                borderColor: '#F5F5F5',
                                alignSelf: 'center'
                            }}
                            onPress={() => this.openGalleryForProfilePic()}
                        >
                            {
                                this.state.profile_picture == null
                                    ?
                                    <Image
                                        style={{
                                            width: 100,
                                            height: 100,
                                            borderRadius: 50,
                                            alignSelf: 'center'
                                        }}
                                        source={require('../assets/loginSplttyLogo.png')}
                                        resizeMode={'contain'}
                                    />
                                    :
                                    <Image
                                        style={{width: 100, height: 100, borderRadius: 50, alignSelf: 'center'}}
                                        source={{uri: this.state.profile_picture}}
                                    />
                            }

                        </TouchableOpacity>

                        <BoldText style={{
                            fontSize: 21,
                            color: '#fff',
                            letterSpacing: 1,
                            textAlign: 'center',
                            marginVertical: 10
                        }}> Change Photo </BoldText>
                        <View style={{height: 0.75, backgroundColor: '#F5F5F5', marginVertical: 24}}/>

                        <View style={{marginLeft: 24, marginRight: 24}}>
                            <View style={{flexDirection: 'row'}}>
                                <Image
                                    style={{width: 18, height: 18, resizeMode: 'contain'}}
                                    source={require('../assets/UserProfile/user.png')}
                                />
                                <BoldText style={{
                                    fontSize: 16,
                                    color: '#fff',
                                    letterSpacing: 1,
                                    textAlign: 'left',
                                    paddingHorizontal: 10
                                }}> My Information </BoldText>
                            </View>

                            <Hoshi
                                label={'First Name'}
                                borderColor={'#F5F5F5'}
                                style={{flex: 1, marginTop: 10}}
                                labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                onChangeText={(first_name) => this.props.typeFirstName(first_name)}
                            />
                            <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.first_name ? this.props.errors.first_name : null}</LightText>

                            <Hoshi
                                label={'Last Name'}
                                borderColor={'#F5F5F5'}
                                style={{flex: 1, marginTop: 10}}
                                labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                onChangeText={(last_name) => this.props.typeLastName(last_name)}
                            />
                            <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.last_name ? this.props.errors.last_name : null}</LightText>

                            <Hoshi
                                label={'Description'}
                                borderColor={'#F5F5F5'}
                                style={{flex: 1, marginTop: 10}}
                                labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                onChangeText={(description) => this.props.typeDescription(description)}
                            />

                            {/*Gender specfic*/}
                            <View style={{flexDirection: 'row', marginTop: 40}}>
                                <Image style={{height: 18, width: 18}}
                                       resizeMode='contain'
                                       source={require('../assets/UserProfile/ion_android_more_vertical_Ionicons.png')}
                                />
                                <BoldText style={{
                                    marginLeft: 6,
                                    fontSize: 15,
                                    color: '#fff',
                                    letterSpacing: 1,
                                    textAlign: 'left'
                                }}> Gender: </BoldText>
                                <TouchableOpacity
                                    style={{width: 100, flexDirection: 'row'}}
                                    onPress={() => this.props.selectGender('Male')}
                                >
                                    <BoldText style={{
                                        marginLeft: 22,
                                        fontSize: 15,
                                        color: this.props.gender === 'Male' ? 'white' : '#AE9CDC',
                                        letterSpacing: 1,
                                        textAlign: 'left'
                                    }}> Male </BoldText>
                                    <Image
                                        style={{height: 18, width: 18, marginLeft: 6}}
                                        resizeMode='contain'
                                        source={(this.props.gender === 'Male' ? require('../assets/UserProfile/check_box.png') : require('../assets/UserProfile/Gender_Oval.png'))}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{width: 100, flexDirection: 'row'}}
                                    onPress={() => this.props.selectGender('Female')}
                                >
                                    <BoldText style={{
                                        marginLeft: 22,
                                        fontSize: 15,
                                        color: this.props.gender === 'Female' ? 'white' : '#AE9CDC',
                                        letterSpacing: 1,
                                        textAlign: 'left'
                                    }}> Female </BoldText>
                                    <Image
                                        style={{height: 18, width: 18, marginLeft: 6}}
                                        resizeMode='contain'
                                        source={(this.props.gender === 'Female' ? require('../assets/UserProfile/check_box.png') : require('../assets/UserProfile/Gender_Oval.png'))}
                                    />
                                </TouchableOpacity>
                            </View>


                            <Hoshi
                                label={'E-mail Address'}
                                borderColor={'#F5F5F5'}
                                style={{flex: 1, marginTop: 30}}
                                labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                onChangeText={(email) => this.props.typeEmail(email)}
                                keyboardType={'email-address'}
                            />
                            <LightText style={{ color: 'red', marginTop: 5 }}>{this.props.errors.email ? `Email ${this.props.errors.email}` : null}</LightText>

                            <Hoshi
                                label={'Phone No.'}
                                borderColor={'#F5F5F5'}
                                style={{flex: 1, marginTop: 30}}
                                labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                editable={false}
                                value={this.props.phone_number}
                            />

                            <Hoshi
                                label={'City'}
                                borderColor={'#F5F5F5'}
                                style={{flex: 1, marginTop: 20}}
                                labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                onChangeText={(city) => this.props.typeCity(city)}
                            />

                            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 20}}>
                                <Hoshi
                                    label={'State'}
                                    borderColor={'#F5F5F5'}
                                    style={{flex: 1}}
                                    labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                    inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                    onChangeText={(state) => this.props.typeState(state)}
                                />

                                <View style={{width: 24}}/>

                                <Hoshi
                                    label={'Country'}
                                    borderColor={'#F5F5F5'}
                                    style={{flex: 1}}
                                    labelStyle={{color: '#AE9CDC', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                    inputStyle={{color: '#FFFFFF', fontSize: 15, fontFamily: "Nexa-Bold"}}
                                    onChangeText={(country) => this.props.typeCountry(country)}
                                />

                            </View>
                        </View>


                        <View style={{height: 0.75, backgroundColor: '#F5F5F5', marginVertical: 24}}/>

                        <View>
                            <View style={{flexDirection: 'row'}}>
                                <Image
                                    style={{width: 20, height: 20, resizeMode: 'contain', marginLeft: 24}}
                                    source={require('../assets/UserProfile/favorite_border.png')}
                                />
                                <BoldText style={{
                                    fontSize: 18,
                                    color: '#fff',
                                    letterSpacing: 1,
                                    textAlign: 'left',
                                    paddingHorizontal: 8
                                }}> My Interests </BoldText>
                            </View>

                            <FlatList
                                data={interests}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item}) => {
                                    return <TouchableOpacity
                                        style={{ alignItems: 'center', justifyContent: 'center' }}
                                        onPress={() => {
                                            this.props.updateInterests(this.props.interests, item.name);
                                            this.setState({
                                                temp: this.state.temp + 1
                                            });
                                        }}
                                    >
                                        <View style={{ height: 45, width: 45, borderWidth: 1, borderColor: 'white', borderRadius: 22.5, alignItems: 'center', justifyContent: 'center', backgroundColor: this.props.interests.indexOf(item.name) > -1 ? '#FFFFFF' : '#9a989c' }}>
                                            <Image
                                                source={item.image}
                                                style={{ height: 30, width: 30 }}
                                                resizeMode={'contain'}
                                            />
                                        </View>
                                        <LightText style={{ textAlign: 'center', color: 'white', marginTop: 5, fontSize: 12 }}>{item.name}</LightText>
                                    </TouchableOpacity>
                                }}
                                horizontal={true}
                                style={{ marginTop: 10, marginHorizontal: 20 }}
                                ItemSeparatorComponent={() => {
                                    return <View style={{ width: 20 }} />
                                }}
                            />
                        </View>


                        {/*Verification UI*/}

                        <View style={{height: 0.75, backgroundColor: '#F5F5F5', marginVertical: 24}}/>
                        <View style={{marginLeft: 24, marginRight: 24}}>
                            <View style={{flexDirection: 'row'}}>
                                <Image
                                    style={{width: 20, height: 20, resizeMode: 'contain'}}
                                    source={require('../assets/UserProfile/verification.png')}
                                />
                                <BoldText style={{
                                    fontSize: 18,
                                    color: '#fff',
                                    letterSpacing: 1,
                                    textAlign: 'left',
                                    paddingHorizontal: 8
                                }}> Verification Entities </BoldText>
                            </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                <Image
                                    source={require('../assets/profile/passport.png')}
                                    style={{height: 25, width: 25}}
                                    resizeMode={'contain'}
                                />
                                <LightText style={{color: '#CBCBCB', marginLeft: 20}}>Passport</LightText>

                                <View style={{flex: 1, alignItems: 'center'}}>
                                    {this.state.passport_picture !== null ?
                                        <Image
                                            style={{height: 40, width: 40, borderRadius: (40 / 2)}}
                                            resizeMode='contain'
                                            source={{uri: this.state.passport_picture}}
                                        />
                                        :
                                        null
                                    }
                                </View>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.openGalleryForPassport()
                                    }}
                                >
                                    <Ionicon name={'ios-camera'} size={30} color={'#FFFFFF'}/>
                                </TouchableOpacity>

                            </View>
                            <View style={{height: 1, backgroundColor: '#CBCBCB', marginVertical: 10}}/>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20}}>
                                <Image
                                    source={require('../assets/profile/facebook.png')}
                                    style={{height: 25, width: 25, position: 'absolute'}}
                                    resizeMode={'contain'}
                                />

                                <Hoshi
                                    label={'Facebook'}
                                    borderColor={'#F5F5F5'}
                                    style={{flex: 1, paddingLeft: 30}}
                                    labelStyle={{
                                        color: '#AE9CDC',
                                        fontSize: 15,
                                        fontFamily: "Nexa-Bold",
                                        paddingLeft: 25
                                    }}
                                    inputStyle={{
                                        color: '#FFFFFF',
                                        fontSize: 15,
                                        fontFamily: "Nexa-Bold",
                                        paddingLeft: 25
                                    }}
                                    onChangeText={(facebook_username) => this.props.typeFacebook(facebook_username)}
                                />
                            </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                <Image
                                    source={require('../assets/profile/instagram.png')}
                                    style={{height: 20, width: 20, position: 'absolute'}}
                                    resizeMode={'contain'}
                                />

                                <Hoshi
                                    label={'Instagram'}
                                    borderColor={'#F5F5F5'}
                                    style={{flex: 1, paddingLeft: 30}}
                                    labelStyle={{
                                        color: '#AE9CDC',
                                        fontSize: 15,
                                        fontFamily: "Nexa-Bold",
                                        paddingLeft: 25
                                    }}
                                    inputStyle={{
                                        color: '#FFFFFF',
                                        fontSize: 15,
                                        fontFamily: "Nexa-Bold",
                                        paddingLeft: 25
                                    }}
                                    onChangeText={(instagram_username) => this.props.typeInstagram(instagram_username)}
                                />
                            </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20}}>
                                <Image
                                    source={require('../assets/profile/linkedin.png')}
                                    style={{height: 20, width: 20, position: 'absolute'}}
                                    resizeMode={'contain'}
                                />

                                <Hoshi
                                    label={'Linkedin'}
                                    borderColor={'#F5F5F5'}
                                    style={{flex: 1, paddingLeft: 30}}
                                    labelStyle={{
                                        color: '#AE9CDC',
                                        fontSize: 15,
                                        fontFamily: "Nexa-Bold",
                                        paddingLeft: 25
                                    }}
                                    inputStyle={{
                                        color: '#FFFFFF',
                                        fontSize: 15,
                                        fontFamily: "Nexa-Bold",
                                        paddingLeft: 25
                                    }}
                                    onChangeText={(linkedin_username) => this.props.typeLinkedin(linkedin_username)}
                                />
                            </View>

                        </View>

                        <TouchableOpacity
                            style={{
                                paddingVertical: 10,
                                alignItems: 'center',
                                borderRadius: 5,
                                borderWidth: 1,
                                borderColor: 'white',
                                margin: 20
                            }}
                            onPress={() => {
                                this.props.updateProfile(this.props.first_name, this.props.last_name, this.props.description, this.props.email, this.props.city, this.props.state, this.props.country, this.props.gender, this.state.profile_picture, this.state.passport_picture, this.props.interests, this.props.facebook_username, this.props.instagram_username, this.props.linkedin_username, this.props.navigation)
                            }}
                        >
                            <BoldText style={{fontSize: 18, color: 'white'}}>Save</BoldText>
                        </TouchableOpacity>
                    </KeyboardAwareScrollView>
                    <Loader loading={this.props.loading}/>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        email: state.user.email,
        first_name: state.user.first_name,
        last_name: state.user.last_name,
        city: state.user.city,
        state: state.user.state,
        country: state.user.country,
        description: state.user.description,
        gender: state.user.gender,
        loading: state.user.loading,
        uuid: state.user.uuid,
        phone_number: state.user.phone_number,
        facebook_username: state.user.facebook_username,
        instagram_username: state.user.instagram_username,
        linkedin_username: state.user.linkedin_username,
        interests: state.user.interests,
        errors: state.user.errors
    }
};

const mapDispatchToProps = dispatch => {
    return {
        typeFirstName: first_name => dispatch(typeFirstName(first_name)),
        typeLastName: last_name => dispatch(typeLastName(last_name)),
        typeDescription: description => dispatch(typeDescription(description)),
        typeEmail: email => dispatch(typeEmail(email)),
        typeCity: city => dispatch(typeCity(city)),
        typeState: state => dispatch(typeState(state)),
        typeCountry: country => dispatch(typeCountry(country)),
        selectGender: gender => dispatch(selectGender(gender)),
        typeFacebook: facebook_username => dispatch(typeFacebook(facebook_username)),
        typeInstagram: instagram_username => dispatch(typeInstagram(instagram_username)),
        typeLinkedin: linkedin_username => dispatch(typeLinkedin(linkedin_username)),
        updateProfileFromServer: data => dispatch(updateProfileFromServer(data)),
        updateProfile: (first_name, last_name, description, email, city, state, country, gender, profile_image, passport, interests, facebook_username, instagram_username, linkedin_username, navigation) => updateProfile(first_name, last_name, description, email, city, state, country, gender, profile_image, passport, interests, facebook_username, instagram_username, linkedin_username, navigation, dispatch),
        updateInterests: (interests, new_interest) => updateInterests(interests, new_interest, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CompleteProfile);
