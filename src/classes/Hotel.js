import React, {Component} from "react";
import {
    Dimensions,
    Image,
    ImageBackground,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import StarRating from 'react-native-star-rating';
import Header from "./../components/Header";
import { BoldText,
    LightText
 } from '../components/styledTexts';

let height1 = Dimensions.get('window').height;

export default class Hotel extends Component {
    render() {
        return (
            <ImageBackground
                style={{flex: 1}}
                source={{uri: this.props.navigation.state.params.item.item.images[0]}}
                resizeMode={'cover'}
            >
            
                    <Header
                        showBackButton={true}
                        backFunc={true}
                    />
                   
                        <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'flex-end' }}/>
                        <LinearGradient 
                            colors={['rgba(255,255,255,0)','rgba(0,0,0,0.5)', 'rgba(0,0,0,1)', 'rgba(0,0,0,1)' ]} 
                            style={styles.linearGradient}
                         >
                    <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'flex-end' }}>
                        <BoldText
                            style={{
                                fontSize: 30,
                                color: '#fff',
                                fontWeight: '600',
                                textShadowColor: 'black',
                                textShadowOffset: { height: 0.5, width: 0.5 },
                                textShadowRadius: 1
                            }}
                        >
                            {this.props.navigation.state.params.item.item.name}
                        </BoldText>

                        <View style={{ flexDirection: 'row', marginVertical: 8}}>
                            <BoldText style={{ color: 'white' }}>
                                299.00$ 
                            </BoldText>
                            <BoldText style={{ color: 'white', marginHorizontal: 10, textDecorationLine: 'line-through' }}>
                                 499.00$
                            </BoldText>
                        </View>
                        <View style={{
                            marginVertical: 15,
                            justifyContent: 'center',
                            height: 26,
                            width: 100,
                            borderRadius: 15,
                            backgroundColor: 'white'
                        }}>
                            <BoldText style={{ textAlign: 'center', color: '#BF68D4', fontSize: 10 }}>
                                2 Km Away
                            </BoldText>
                           
                        </View>

                        <View style={{height: 1, backgroundColor: '#fff', opacity: .5}}/>
                        <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                            <Image
                                source={require('../assets/weather_icon.png')}
                            />

                            <View>
                                <BoldText style={{ color: '#fff', fontSize: 20, fontWeight: '500' }}>
                                    24.0°C
                                </BoldText>
                                <BoldText style={{ color: 'rgba(255,255,255,0.6)', fontSize: 10 }}>
                                    SUNNY
                                </BoldText>
                            </View>
                            <View style={{
                                width: 3,
                                height: 25,
                                backgroundColor: '#fff',
                                opacity: .2
                            }}/>
                            <View style={{}}>
                                <View style={{ flexDirection: 'row' }}>
                                    <BoldText style={{color: '#fff', fontSize: 20, fontWeight: '500'}}>
                                     4.6
                                    <BoldText style={{color: 'rgba(255,255,255,0.6)', fontSize: 13 }}>
                                        (1.6k)
                                    </BoldText>
                                 </BoldText>
                                </View>
                               
                                <StarRating
                                    disabled={true}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    starSize={12}
                                    width={50}
                                    rating={4}
                                    fullStarColor={'#EFB824'}
                                />
                               
                            </View>

                            <TouchableOpacity
                                style={{
                                    marginLeft: height1 < 700 ? 10 : 20,
                                    marginBottom: height1 < 700 ? 45 : 10,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}
                                onPress={() => this.props.navigation.navigate('HotelInfo', {item: this.props.navigation.state.params.item})}
                            >
                                <Image
                                    style={{paddingBottom: 20}}
                                    source={require('./../assets/drag.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
            </ImageBackground>
        );
    }
}


var styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        // paddingLeft: 15,
        // paddingRight: 15,
        // borderRadius: 5
    }
});
