import React, {Component} from 'react';
import {ImageBackground, SafeAreaView,StatusBar} from 'react-native';
import {AppContainer} from './src/navigation/Router';
import {Provider} from 'react-redux';
import configureStore from './src/store/configureStore';
import LinearGradient from 'react-native-linear-gradient';

const store = configureStore();

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <LinearGradient colors={['rgba(59, 41, 176, 1)', 'rgba(175, 75, 207, 1)', 'rgba(168, 70, 209, 1)']} style={{flex: 1}}>
                 <SafeAreaView style={{ flex: 1 }}>
                    <ImageBackground style={{flex: 1}} source={require('./src/assets/background.png')}>
                        <AppContainer />
                    </ImageBackground>
                 </SafeAreaView>
                </LinearGradient>
            </Provider>
        );
    }
}
